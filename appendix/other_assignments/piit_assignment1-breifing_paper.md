# Continuous Integration

## Abstract

_Software applications are not just stand-alone systems anymore. They have become a suite of interconnect heterogeneous components that execute or encapsulate data, whilst being dispersed over a computer network. This often makes it difficult for developers to integrate new features and maintain old ones. This is because any changes to an existing code base is met with resistance, out of possible fears of breaking the application. This paper explores the process of "Continous Integration" practice and its benefits over traditional development methods._

## Introduction

"The process of integrating software is not a new problem" (M. Duvall, P 2007). For a one-person project with few external dependencies software integration can be a non-issue, however as a project's complexity increases there is a greater need to ensure that software components can integrate and work together. Leaving this process until the end of a project leads to, often costly, software quality problems and delays. Continuous Integration (CI) helps address these issues quicker, in small increments and reduces risk overall.

"[CI] is a software development practice" (Fowler, M 2006), which requires team members to frequently checking in and testing changes. Each integration with the main project base triggers automated tests and build procedures. This process detects errors as soon as possible to ensure that small bugs are caught and the application can be built early on. This approach often leads to significantly reduced integration problems and helps teams to quickly develop cohesive software.

The term "Continuous Integration" originated from the book "Extreme Programming Explained" by Kent Beck. Beck briefly described how that the process of integrating and testing code should only take a few hours, or a day of development at most. Whilst suggesting that a machine should be dedicated to integration, which when available developers could use it compare changes and run tests until successful.

Continuous Integration is often confused with Continuous Delivery and Continuous Deployment. Continuous Delivery is a software development practice that ensures code can always be deployed to production. Continuous Deployment extends that practice and automatically deploys code with every change that has successfully passed the automated tests. The problem all these practices combat is the flaws and faults of traditional development techniques.

## Versus Tradition

Traditional methods, such as waterfall development, usually result in late and/or buggy software. "Most developers and IT professionals know this, and are justifiably leery of speeding things up" (Puppet Labs, 2013). Traditional methods are not equipped to test software frequently and thoroughly enough, often leading to code quality overall.

"Traditional software development methods don't dictate how frequently or regularly you integrate all of the source on a project" (Version One 2014). Programmers will often work separately for long periods of time on the same source without realizing the amount of conflicts and potential bugs they are generating. Developers typically find that the process of merging their code with the main project is often slow, due to long conflict resolution and debugging sessions that often occur at the end of a development cycle.

In Continuous Integration "no code is left unintegrated for more than a few hours" (Beck, K 1998). At the end of every development cycle, code is integrated, the application is built and it successfully passes all tests. This quick integration gives many of the benefits of both being single developer and instantaneous integration.

During development, developers can act like they are the only person on the project. Meaning they can make changes wherever they want to. Then as integrators, they become aware (via their tools) where there are collisions have occurred within the classes or methods. By running the tests they become aware of semantic collisions.

## Benefits of Continuous Integration

"[Continuous Integration] dramatically reduces the risk of the project" (Beck, K 1998) because code is integrated within a short period of time, the chances of conflicts go down. However, "If two people have different ideas about the appearance or operation of a piece of code, you will know in hours" (M. Duvall, P 2007) and can resolve any issues that arise.

Although Continuous Integration is a practice its process has evolved to incorporate or be implemented by various tools. A CI tool will regular check the main project base for changes (often through a version control system), build the application and run automated tests. Each build will then produce reports on whether tests passed or failed.

"The most common used CI tools are Jenkins, Hudson, Bamboo and CruiseControl." (Puppet Labs 2013)

The setting up of a Continuous Integration system can be quite complex, sometimes even requiring full-time staff to monitor and maintain it. However, the increased overhead in maintaining the CI system "is usually a misguided perception, because the need to integrate, test, inspect, and deploy exists regardless of whether [developers] are using CI. Managing a robust CI system is better than managing manual processes" (M. Duvall, P 2007).

### Maintain a Single Source Repository.

"Simply put, you must use a version control repository in order to perform CI" (M. Duvall, P 2007). "These tools - called Source Code Management tools, configuration management, version control systems, repositories, or various other names - are an integral part of most development projects" (Martin Fowler, 2006). As software projects include more interconnect components and keeping track of all these files is essential, especially when multiple people are involved.

### Automate the Build

"[The] build is automatically triggered whenever source code is checked in to the repository" (Kawalerowicz, M & Berntson, C 2011). "Automation allows you to make successful processes repeatable" (Puppet Labs 2013). Getting sources to compile can often be a complicated process and error prone when done manually. Because the build can potentially ran several times a day automation allows you make the changes quickly and safely, without introducing the human errors.

Automation in "Continuous Integration reduces the amount of repetitive processes the developers need to perform when building or deploying the software thus reducing the risks of late discovery of defects" (Bergmann, S 2011).

### Collective Ownership

"Integration is primarily about communication. Integration allows developers to tell other developers about the changes they have made" (Fowler, M 2006). Frequent communication allows people to know quickly as changes develop.

"[You] ensure that everyone can easily see the state of the system and the changes that have been made to it" (Fowler, M 2006). This constant integration encourages developers to change and improve the overall quality of software, it helps build a collective ownership of project.

"Collective ownership encourages the whole team to make the whole system better" (Beck, K 1998). This affects the code complexity of the project, because everyone is looking at it. Complex code cannot live for very long because when it is found, someone will try to simplify it. However, even if the simplification fails, as evidenced by the tests failing, the code can be thrown away and there will be someone who understands why the code is complex. More often than not the simplification works, which reduce the complexity of project and improves that software quality is better overall.

### Establish Greater Product Confidence

“Overall, effective application of CI practices can provide greater confidence in producing a software product” (M. Duvall, P 2007). Every build of the project the team is build confidence because the tests to verify software behaviour, ensuring that the project meeting its coding and design standards. It also means that “once the commit build is good then other people can work on the code with confidence” (Fowler, M 2006).

Without frequent integrations, developers can feel stifled because they unaware of the impact their code may change. However, “since a CI system can inform you when something goes wrong, developers and other team members have more confidence in making changes” (M. Duvall, P 2007). There is greater confidence in the project quality because CI encourages a single-source repository from which all software assets are built.

## Conclusion

The result of practicing "Continuous Integration" ensures that there is a stable software deliverable, works correctly and contains few bugs. Every developers works with the same main project base and never develops the software in isolation long enough that it is difficult to integrate. Less time is spent finding bugs, because they found and resolved early on. Overall making integration a non-event.


# References

Carzaniga, A, Fuggetta, A, Hall, R, Heimbigner, D, Hoek, A, & Wolf, A 1998, 'A Characterization Framework for Software Deployment Technologies'

Fowler, M 2006, 'Continuous Integration' <http://martinfowler.com/articles/continuousIntegration.html> [Accessed 23 August 2014]

Beck, K 1998, 'Extreme Programming Explained'

Puppet Labs 2013, Continuous Delivery: What it is & How to get started

M. Duvall, P 2007, Continuous Integration: Improving Software Quality and Reducing Risk

Kawalerowicz, M & Berntson, C 2011, 'Continuous Integration in .NET'

Version One 2014, 'Continuous Code Integration In Agile Software Development' <http://www.versionone.com/Agile101/Continuous_Integration.asp> [Accessed 23 August 2014]

M. Berg, A 2012, 'Jenkins Continuous Integration Cookbook'

Bergmann, S 2011, 'Integrating PHP Projects with Jenkins'
