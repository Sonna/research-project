# Upload JSON reuest CURL command example

```shell
curl -i -u jeffmccune -d '
{
  "name": "web",
  "active": true,
  "events": ["pull_request", "issues"],
  "config": {
    "url": "http://event.endpoint/event/github",
    "secret": "845f1e81063d2808823abc810f45cca2b329b87fd5e81d6649219ed6e0560e12"
    "content_type": "json"
  }
}' https://api.github.com/repos/puppetlabs/puppet/hooks
```
