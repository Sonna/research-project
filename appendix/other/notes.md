# Meeting minutes notes 2014/09/02

Q: Can team GitHub managers please add the account “Sonna-Bot” to their teams and repositories? _This is so I can start setting up continuous build from a server for my research project._

## alternative release builds
Q: How do we want deploy any builds made by Jenkins for access and testing?

- Google Docs
- GitHub Releases + GitHub Pages

Does anyone have experience with using and setting up SSH tunneling or equivalent for accessing a computer behind a router/firewall?

- Leave it for now

## Q/A
What time are people available during the week for Q/A?

- Prepare an Online suvery
