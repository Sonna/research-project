# Commits Leaderboard

| Author | Commits | Lines added | Lines deleted | Sum of Lines per Commit |
|--------|---------|-------------|---------------| -- |
| Alex Sonneveld <Alex.Sonneveld@live.com.au> | 496 | 298692 | 235591 | 127.219758065 |
| Hadleigh B-A <hba1990@gmail.com> | 467 | 1944885 | 1000692 | 2021.826552463 |
| Anthony Chenevier <anthony.chenevier@gmail.com> | 162 | 1989662 | 1070955 | 5671.030864198 |
| Emile Pascoe <emile.pascoe@gmail.com> | 110 | 278757 | 37650 | 2191.88181818182 |
| Milton Plotkin <plotkinmilton@gmail.com> | 105 | 399099 | 140087 | 2466.78095238095 |
| Rybo5000 <mentalrybo@hotmail.com> | 95 | 878820 | 92183 | 8280.38947368421 |
| Ciaran Blewitt <blewittc@gmail.com> | 80 | 6316 | 1321 | 62.4375 |
| jameskernaghan <james.kernaghan@live.com.au> | 61 | 5944 | 9931 | -65.3606557377049 |
| Christopher Forrest <zen129318@gmail.com> | 49 | 47102 | 49471 | -48.3469387755102 |
| hughxsaunders <hughxsaunders@gmail.com> | 14 | 1103 | 189 | 65.2857142857143 |
| Ryan Hisheh <rhisheh@live.com> | 11 | 6711605 | 1956992 | 432237.545454545 |
| Daniel Clements <d.clements@live.com.au> | 7 | 17 | 2 | 2.14285714285714 |
| Sonna-Bot <Alex.J.Sonneveld+GitHub.Bot@gmail.com> | 6 | 46 | 57 | -1.83333333333333 |
| Clinton Woodward <cwoodward@swin.edu.au> | 3 | 12 | 0 | 4 |
| cactuarzero <johnnymacdon@hotmail.com> | 1 | 60691 | 0 | 60691 |

