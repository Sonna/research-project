# Notes

- What will you be able ask the Developers?

- Would have the Problem Statement earlier...
- Discussion of the validation and opinion of their solution or other solutions.
- Separate Facts from Opinions in the discussion at the end of the report
- The Question "more about the people using CI" rather than "how CI is done and should be done"

- [ ] Get the basic setup up and running by tonight [2014-09-01]

- The Paper communicates the research and the method used to communicated it

- "Should no surpises throughout a paper, all information should be explained and why this information is relavant"

- Describing the Context, who & what others have done before and what you are researching

- Discussion should contain your opinion of the results

- [ ] Write up a series of questions that I should be asking developers as a part of this research:
    - What is your previous experience in Software Development?
    - What environment do you do your programming in?

