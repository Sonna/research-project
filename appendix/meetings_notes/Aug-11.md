# Meeting notes

- focus on how developers interact with continuous integration service if they 
  are aware of it and how they interact with it adn whether or not their habits  
  will inmprove over time

- this should consider the behaviours of developers and how they change over time
  when interacting with the system

- define the abstract proposals
    - Describe what it takes to set it up and what needs to occur

- look into "thematic approach"
  - e.g. interview the developers to collect information and to find common themes

- Need to define at least three or more abstracts (at least one main idea)
  - [domain, gap, approach, outcome]

- "What profiler and automation tools Jenkins can use to create reports"
    - write a report on those, DO NOT have to implement (yet)

- Find a research paper on your research area and analyse

- Software module diagram
- Hardware architecture diagram
- Activity/Process diagram of User interacting with the software


# Notes for next time

- record the meeting next time
- bring a paper to discuss for next meeting
