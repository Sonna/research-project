Write a list things to do, reorder by proirity and send a copy via email to Clinton

Tasks that [Need to| Could be] done:

_How long will it take (1,2,4,8) roughly in hours/difficulty._

- **Must:**
  - [x] - 1 - Download latest copy of the survey data
    - [x] - 1 - Find out who has not finished the survey,
      _(which was sadly no-one else had attempt since it was last download over a fornight ago)_ and

    - [x] - 1 - Notify people to complete the survey
      _Notified people via Facebook Meta-Team group page._

  - [ ] - 4 - Analyse the survey data and then communicate/report
    - [ ] _Talk about the overall setup; i.e. config_
    - [ ] _Talk about the raw data; e.g. like the demographics._
    - [ ] _Describe the experiment._
    - [ ] _Talk about survey._
  - [ ] - 8 - Train the designers to use Git/GitHub/SourceTree
    - TBD (Break it down)

- **Should:**
  - [ ] - 4 - Implement testing for Unity projects

  - [x] - 1 - Backup a local copy of Local Mac Mini Jenkin builds config and
    log to the EC2 instance

    _This worked, kind of._

    _The major was the credentials were not synced correctly between
    servers and broke the majority of the Jenkins Jobs. Otherwise, the
    online server has all the history of the previous builds._

  - [ ] - 4 - Package and deploy compiled Unity Project binarys to either:
      - [ ] - 1 - GitHub Release,
      - [ ] - 2 - DropBox, and/or
      - [ ] - 4 - GoogleDrive

      - [x] - 2 - Amazon S3 Bucket

        _It currently deploys the build results onto a personal AWS S3
        bucket and links them from within Jenkins. However, the links
        within Jenkins do not work properly (possibly because of spaces
        in the Job name, but currently unsure)._

        _The S3 bucket is available at the following link, but it is
        currently impossible to traverse._

        http://gameslab-jenkins-s3-uploads.s3-website-ap-southeast-2.amazonaws.com

- **Could:**
  - [ ] - 2 - Setup SonarQube on Amazon Web Services

  - [x] - 2 - Use Jenkins Job "Template Project Plugin" to refactor the jobs
    into a single reusable project

    _I attempted this both with Jenkins **Parameterised Builds** and **EZ-
    Templates**. However, neither worked as expected._

    _**Parameterised Builds** sort of works, but:
      - Slack notifications break on parameterised build, because
        environment variables are not evaluated before it is called and
        thus fails the job.

      - The workspace is rebuilt every time for every new project; which
        means that it replaces the workspace every time it needs to build
        a different project and effectively collapses the multiple jobs into
        one job, but as expected._

    _**EZ-Templates** instead works as a reverse inheritance job, whereas
    the **Parameterised Build** acts a inheritance-like, but allows new
    tasks to be added to the job. This defeats the purpose I wanted it
    for, which would a mix of both these solutions where the user can
    build one job that acts the default template that can be reused, have
    new tasks added to it and override the parameters already given,
    whilst continuing to inheriting properties the the initial job (
    instead of just copying them). However, that sort of describes the
    default job system already present in Jenkins._

- **Won't:**
  - [ ] - 4 - Setup the Mac Mini as a "Java Web app" slave (to avoid the "behind an NAT/firewall" issue)

  - [x] - 2 - Configure the Nginx server to forward the Jenkins server to a `ci.` subdomain

    _I attempted to set this up, but currently it just redirects to the
    default URL `sonnahq.com`._


## Notes:

- That nobody understood the question about "Continuous Integration" (Semester 2 - Week 6 meta-team meeting minutes notes)
