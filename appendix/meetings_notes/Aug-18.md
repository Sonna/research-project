# August 18 [15:30] - Meeting notes

> _Reading aloud example of an research abstracts_
> [Clinton] 

- Why "are you telling about <insert topic>"?

- Write and abstract that explores [Domain, Question, Method, Outcome]

Domain:
    - Designers
    - Programmers
    - Game development

Question:
    - "How it can be used" (like a spec requirement)
    - "How it can used and how it can help"
    - "How it is used" (Pros and cons)
    - "How can CI environment help developers and designers"
    - How does it benefit?
    - How does Continuous Integration environment effect a development team workflow (and including the culture)?
        + e.g. "You do not want to break the daily build"

Method:
    "To implement and observe an development team using an Continuious Integration environment to build a Game with Unity3D & anaylse the results"

Outcome:
    "After training and explaining to the developerrs and programmers and explore how the use it. "Your Opinion""
    - Identity the benefits and disadvantages
    - Guidelines for building, look out for, measure, ...


- TASK: Look into papers on what you researching and bring it into next meeting
- Abstract polish
- Write a Spec: What the tool is supose to?


# References

[UW-Madison]: http://writing.wisc.edu/Handbook/presentations_abstracts.html
> ## What is an abstract?
> 
> An abstract is a concise summary of a larger project (a thesis, research report, performance, service project, etc.) that concisely describes the content and scope of the project and identifies the project’s objective, its methodology and its findings, conclusions, or intended results.
> 
> Remember that your abstract is a description of your project (what you specifically are doing) and not a description of your topic (whatever you’re doing the project on).  It is easy to get these two types of description confused.  Since abstracts are generally very short, it’s important that you don’t get bogged down in a summary of the entire background of your topic. 
> 
> As you are writing your abstract, stop at the end of every sentence and make sure you are summarizing the project you have undertaken rather than the more general topic. 


[UCDavis]: http://undergraduateresearch.ucdavis.edu/urcConf/write.html
> # What is an abstract?
> 
> An abstract is a one-paragraph summary of a research project. Abstracts precede papers in research journals and appear in programs of scholarly conferences. In journals, the abstract allows readers to quickly grasp the purpose and major ideas of a paper and lets other researchers know whether reading the entire paper will be worthwhile. In conferences, the abstract is the advertisement that the paper deserves the audience's attention.
