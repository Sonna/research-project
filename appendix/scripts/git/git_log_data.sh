git log --pretty=format:"%s %b" --no-merges >> git_log.txt

git log --pretty=format:"%s" --no-merges >> git_summary_log.txt
git log --pretty=format:"%b" --no-merges >> git_body_log.txt

# Alternative methods
# git log --pretty=format:"%h, %s, %b" >> git_log.txt
# git log --pretty=format:"%h, %s, %b" --no-merges >> git_log.txt

# awk '{a[$1]++}END{for(k in a)print a[k],k}' git_log.txt | sort > out.txt
