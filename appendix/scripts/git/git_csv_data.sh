# Get Git log into a CSV format with shortstat data (modifications, additions, deletions)
#
# Initialise CSV files
echo "Commit Hash, Date, Author, Subject, Modified Files, Added Lines, Deleted Lines \n" > _git_shortstat_frequency_log.csv
echo "Commit Hash, Date, Author, Subject, Modified Files, Added Lines, Deleted Lines \n" > temp_log_merges.csv
echo "Commit Hash, Date, Author, Subject, Modified Files, Added Lines, Deleted Lines \n" > temp_log_wihtout_merges.csv

# Get oneline git log into CSV format (does not include merge commits; e.g. "Merged Pull Request" commit)
git log --pretty=format:'"%h","%ai","%aN","%s",' --shortstat --no-merges | paste - - - >> _git_shortstat_frequency_log.csv

# Remove unnncessary `shortstat` words; e.g. "files changed", "insertions" and" "deletions"
sed -i '' \
    -e 's/file changed//g' \
    -e 's/files changed//g' \
    -e 's/insertion(+)//g' \
    -e 's/insertions(+)//g' \
    -e 's/deletion(-)//g' \
    -e 's/deletions(-)//g' \
    -e '/^$/d' \
    _git_shortstat_frequency_log.csv

git log --pretty=format:'"%h","%ai","%aN","%s",' >> temp_log_merges.csv
git log --pretty=format:'"%h","%ai","%aN","%s",' --no-merges >> temp_log_wihtout_merges.csv

# Gets the missing "Merge Pull Request" rows
diff --unchanged-line-format="" temp_log_wihtout_merges.csv temp_log_merges.csv > temp_missing_merged_commits.csv

# Concat the "missing merge commits" with the log without "merge commits", but has code frequency values
cat _git_shortstat_frequency_log.csv temp_missing_merged_commits.csv > _git_log.csv

# Cleanup; i.e. remove temporary files
rm temp_log_merges.csv
rm temp_log_wihtout_merges.csv
rm temp_missing_merged_commits.csv
