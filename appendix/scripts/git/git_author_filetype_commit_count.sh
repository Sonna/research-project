# References:
# - http://gitready.com/intermediate/2009/01/22/count-your-commits.html
# - http://stackoverflow.com/questions/12887580/find-all-git-commits-for-a-file-type
# - http://stackoverflow.com/questions/15834839/how-can-i-make-git-list-all-changed-files-of-a-certain-type-in-a-specific-path-f
# - http://stackoverflow.com/questions/4259996/how-can-i-view-a-git-log-of-just-one-users-commits

git shortlog -s -- **/*.md --author="Alex"
