#!/usr/bin/env bash

# git log --format='%aN' | awk '{arr[$0]++} END{for (i in arr){print arr[i], i;}}' | sort -rn | cut -d\  -f2-

my_dir="$(dirname "$0")"

PEOPLE=(
  "Ryan Bowen"
  "Milton Plotkin"
  "James Kernaghan"
  "hughxsaunders"
  "Daniel Clements"
  "Sonna-Bot"
  "Alex Sonneveld"
)

for i in "${!PEOPLE[@]}"
do
  # "$my_dir/git_effort.sh"
  # echo "--author \"${PEOPLE[$i]}\""
  # sh "\"$my_dir/git_effort.sh\" --author \"${PEOPLE[$i]}\"" > git_effort_${PEOPLE[$i]}.csv
  "$my_dir/git_effort.sh" --author "${PEOPLE[$i]}" > git_effort_${PEOPLE[$i]// /_}.csv
done
