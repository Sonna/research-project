#!/usr/bin/env bash

# References:
# - https://github.com/tj/git-extras/blob/master/bin/git-effort

tmp=/tmp/.git-effort
above='0'
author=""

#
# get date for the given <commit>
#

date() {
  git log --pretty='format: %ai' $1 | cut -d ' ' -f 2
}

#
# get active days for the given <commit>
#

active_days() {
  date $1 | uniq | awk '
    { sum += 1 }
    END { print sum }
  '
}

#
# compute the effort of the given <file ...>
#

effort() {
  for file in "$@"; do
    commits=`git log --use-mailmap --author=$author --oneline "$file"  | wc -l`

    # ignore <= --above
    test $commits -le $above && continue

    # filename, commits, active days, filesize, lines of code
    active=`active_days "$file"`
    filesize=`stat -f %z "$file"`
    lines_of_code=`cat "$file" | wc -l`
    printf "\"%s\", %d, %d, %d, %d\n" $file $commits $active $filesize $lines_of_code
  done
}

#
# print heading
#

heading() {
  printf "%s, %s, \"%s\", \"%s\", \"%s\"\n" 'file' 'commits' 'active days' 'filesize (bytes)' 'Lines of code'
}

#
# output sorted results
#

sort_effort() {
  clear
  echo
  heading
  cat $tmp | sort -rn -k 2
}

# --author "Committer Name"

if test "$1" = "--author"; then
  shift; author=$1
  shift
fi

# [file ...]

declare -a files=()
if test $# -ge 1; then
  files=("$@")
else
  save_ifs=$IFS
  IFS=`echo -en "\n\b"`
  files=(`git ls-files`)
  IFS=$safe_ifs
  unset save_ifs
fi

# loop files

heading
effort "${files[@]}" | tee $tmp && sort_effort
echo
