gource --user-image-dir .git/avatar/ \
       --hide dirnames,filenames \
       --key \
       --bloom-multiplier 0.5 \
       --bloom-intensity 0.5 \
       --seconds-per-day 0.1 \
       --auto-skip-seconds 1 \
       --title "My Project" \
       --caption-file caption_file.txt \
       --caption-size 32 \
       --caption-colour FFFF00 \
       -1280x720 -o - | \
ffmpeg -y \
       -r 60 \
       -f image2pipe \
       -vcodec ppm \
       -i - \
       -vcodec libx264 \
       -preset ultrafast \
       -pix_fmt yuv420p \
       -crf 1 \
       -threads 0 \
       -bf 0 \
       gource.mp4
