heroku pgbackups:capture
curl -o latest.dump `heroku pgbackups:url`

# pg_restore --verbose --clean --no-acl --no-owner -h localhost -U user -d mydb latest.dump
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U Sonna -d gameslab_survey latest.dump

# rake surveyor:dump SURVEY_ACCESS_CODE=<access_code> [OUTPUT_DIR=<dir>] [SURVEY_VERSION=<survey_version>]
rake surveyor:dump SURVEY_ACCESS_CODE=games-lab-survey-post-project

# Alternatively
# http://localhost:3000/surveys/games-lab-survey-post-project.json
# http://localhost:3000/surveys/games-lab-survey-post-project/[response_set.access_code].json
#
# References:
# - https://github.com/NUBIC/surveyor/wiki/Guide%3A-The-JSON-API-and-Exports
