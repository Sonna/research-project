# References:
# - http://serverfault.com/questions/133000/replace-a-space-char-in-filename-with-an-underscore
for file in *; do [ -f "$file" ] && ( mv "$file" "$(echo $file | sed -e 's/ /_/g')" ); done
