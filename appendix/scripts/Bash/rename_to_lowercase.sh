# References:
# - http://stackoverflow.com/questions/7787029/how-do-i-rename-all-files-to-lowercase
for f in *; do mv "$f" "`echo $f | tr "[:upper:]" "[:lower:]"`"; done
