Last login: Fri Sep  5 10:56:01 on ttys000
sonna-mac-mini:~ sonna$ brew update
Updated Homebrew from 7647bad9 to 18b6f495.
==> New Formulae
jbake	  makeself  ttylog
==> Updated Formulae
app-engine-java-sdk	   ircii		      pazpar2
arangodb		   jenkins		      percona-server
bmon			   leiningen		      phantomjs
boot2docker		   libimobiledevice	      points2grid
cgvg			   libmtp		      proxychains-ng
cmocka			   libssh		      qbs
concurrencykit		   libtins		      redis
couchdb			   libunistring		      riak
djvulibre		   libvirt		      sphinx
dos2unix		   lilypond		      sqlitebrowser
emscripten		   logstash		      suomi-malaga-voikko
fontforge		   logtalk		      svtplay-dl
gcc			   lv			      swi-prolog
git-cola		   mecab-ko		      syncthing
glew			   mecab-ko-dic		      tomcat
gnunet			   mercurial		      transcrypt
go-app-engine-32	   metaproxy		      tree
google-app-engine	   mpd			      unrar
gource			   mpdviz		      vim
graphicsmagick		   mpich2		      xplanetfx
groovy			   mu			      youtube-dl
guile			   node			      zsh
imagemagick		   nvm
sonna-mac-mini:~ sonna$ brew install nginx
==> Installing dependencies for nginx: pcre, openssl
==> Installing nginx dependency: pcre
==> Downloading https://downloads.sf.net/project/machomebrew/Bottles/pcre-8.35.m
######################################################################## 100.0%
==> Pouring pcre-8.35.mavericks.bottle.tar.gz
🍺  /usr/local/Cellar/pcre/8.35: 146 files, 5.8M
==> Installing nginx dependency: openssl
==> Downloading https://downloads.sf.net/project/machomebrew/Bottles/openssl-1.0
######################################################################## 100.0%
==> Pouring openssl-1.0.1i.mavericks.bottle.3.tar.gz
==> Caveats
A CA file has been bootstrapped using certificates from the system
keychain. To add additional certificates, place .pem files in
  /usr/local/etc/openssl/certs

and run
  /usr/local/opt/openssl/bin/c_rehash

This formula is keg-only, which means it was not symlinked into /usr/local.

Mac OS X already provides this software and installing another version in
parallel can cause all kinds of trouble.

The OpenSSL provided by OS X is too old for some software.

Generally there are no consequences of this for you. If you build your
own software and it requires this formula, you'll need to add to your
build variables:

    LDFLAGS:  -L/usr/local/opt/openssl/lib
    CPPFLAGS: -I/usr/local/opt/openssl/include

==> Summary
🍺  /usr/local/Cellar/openssl/1.0.1i: 430 files, 15M
==> Installing nginx
==> Downloading https://downloads.sf.net/project/machomebrew/Bottles/nginx-1.6.1
######################################################################## 100.0%
==> Pouring nginx-1.6.1_1.mavericks.bottle.1.tar.gz
==> Caveats
Docroot is: /usr/local/var/www

The default port has been set in /usr/local/etc/nginx/nginx.conf to 8080 so that
nginx can run without sudo.

To have launchd start nginx at login:
    ln -sfv /usr/local/opt/nginx/*.plist ~/Library/LaunchAgents
Then to load nginx now:
    launchctl load ~/Library/LaunchAgents/homebrew.mxcl.nginx.plist
Or, if you don't want/need launchctl, you can just run:
    nginx
==> Summary
🍺  /usr/local/Cellar/nginx/1.6.1_1: 7 files, 912K
sonna-mac-mini:~ sonna$ sudo nginx
Password:
sonna-mac-mini:~ sonna$ curl localhost
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Server</title>
		<style type="text/css">
		* {		
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			-box-sizing: border-box;
		}
		body, html {
			background-color: #F2F3F4;
			color: #333333;
			font-family: 'Lucida Grande', 'Lucida Sans Unicode', Helvetica, Arial, Verdana, sans-serif;
			font-size: 14px;
			height: 100%;
			line-height: 21px;
			margin: 0px;
			text-align: center;
			word-spacing: -1px;
		}
		#content {
			padding: 26px;
		}
		#main {
			background: #FFFFFF;
			border: 1px solid #D5D5D6;
			border-top-color: #E0E1E2;
			border-bottom-color: #C0C1C2;
			margin: 0px auto 0px auto;
			padding: 100px 0px 100px 0px;
			width: 730px;
			-webkit-box-shadow: 0px 1px 3px rgba(0,0,0,0.1);
			-moz-box-shadow: 0px 1px 3px rgba(0,0,0,0.1);
			box-shadow: 0px 1px 3px rgba(0,0,0,0.1);
			-webkit-border-radius: 6px;
			-moz-border-radius: 6px;
			border-radius: 6px;
		}
		h1, h2 {
			color: #BBBBBB;
			font-family: Helvetica, Arial, Verdana, sans-serif;
			font-size: 18px;
			font-weight: normal;
			line-height: 25px;
			margin: 0px;
			text-align: center;
		}
		</style>
	</head>
	<body>
		<div id="content">
			<div id="main">
				<h1>Websites are turned off.</h1>
				<h2>An administrator can turn them on using the Server application.</h2>
			</div>
		</div>
	</body>
</html>
sonna-mac-mini:~ sonna$ curl localhost:8080
<html>
<head><title>404 Not Found</title></head>
<body bgcolor="white">
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.6.1</center>
</body>
</html>
sonna-mac-mini:~ sonna$ sudo nginx -s stop
sonna-mac-mini:~ sonna$ mkdir {HOME}/nginx
mkdir: {HOME}: No such file or directory
sonna-mac-mini:~ sonna$ mkdir nginx
sonna-mac-mini:~ sonna$ cd nginx/
sonna-mac-mini:nginx sonna$ mkdir root
sonna-mac-mini:nginx sonna$ cd root/
sonna-mac-mini:root sonna$ pwd
/Users/sonna/nginx/root
sonna-mac-mini:root sonna$ sudo mate /usr/local/etc/nginx/nginx.conf
sudo: mate: command not found
sonna-mac-mini:root sonna$ sudo vim /usr/local/etc/nginx/nginx.conf
sonna-mac-mini:root sonna$ sudo nginx
nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
nginx: [emerg] bind() to 0.0.0.0:80 failed (48: Address already in use)
nginx: [emerg] still could not bind()
sonna-mac-mini:root sonna$ apachectl stop
This operation requires root.
sonna-mac-mini:root sonna$ sudo !!
sudo apachectl stop
sonna-mac-mini:root sonna$ sudo nginx
sonna-mac-mini:root sonna$ curl http://127.0.0.1:80
<html>
<head><title>403 Forbidden</title></head>
<body bgcolor="white">
<center><h1>403 Forbidden</h1></center>
<hr><center>nginx/1.6.1</center>
</body>
</html>
sonna-mac-mini:root sonna$ sudo nginx -s stop
sonna-mac-mini:root sonna$ 
