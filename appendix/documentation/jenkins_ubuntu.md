# Installing Jenkins on Ubuntu

**Prerequisites:**
-

1.  To install Jenkins, run the following commands

    ```
wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins
    ```

2.  _The following section is a direct quote._

# Setting up an Nginx Proxy for port 80 -> 8080
This configuration will setup Nginx to proxy port 80 to 8080 so that you can keep Jenkins on 8080. Instructions originally found in a GitHub Gist from [rdegges](https://github.com/rdegges): https://gist.github.com/913102

- **Install Nginx.**

  ```
  sudo aptitude -y install nginx
  ```

- **Remove default configuration.**

  ```
cd /etc/nginx/sites-available
sudo rm default ../sites-enabled/default
  ```
- **Create new configuration for Jenkins.** This example uses cat, but you can use your favorite text editor. Make sure to replace 'ci.yourcompany.com' with your domain name.
Note: Sometimes your permissions (umask, etc) might be setup such that this won't work. Create the file somewhere else then copy it into place if you run into that problem.

  ```
sudo cat > jenkins
upstream app_server {
    server 127.0.0.1:8080 fail_timeout=0;
}

server {
    listen 80;
    listen [::]:80 default ipv6only=on;
    server_name ci.yourcompany.com;

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;

        if (!-f $request_filename) {
            proxy_pass http://app_server;
            break;
        }
    }
}
^D # Hit CTRL + D to finish writing the file
  ```

- **Link your configuration** from sites-available to sites-enabled:

  ```
sudo ln -s /etc/nginx/sites-available/jenkins /etc/nginx/sites-enabled/
  ```

- **Restart Nginx**

  ```
sudo service nginx restart
  ```


3.  Setup Jenkins GitHub SSH permissions.

    On the Ubuntu server login as the Jenkins user

    ```
    su jenkins
    ```

    For Example

    ```
    ubuntu@ip-172-31-15-30:~$ su jenkins
    Password:
    jenkins@ip-172-31-15-30:/home/ubuntu$
    ```

    Then follow [GitHub's guide for generating SSH Keys][Generating SSH Keys], which I will quote here.

# Troubleshooting

## Finding Users on Ubuntu
To list all users you can use:

```
cut -d: -f1 /etc/passwd
```

For example
```
$ cut -d: -f1 /etc/passwd
root
daemon
bin
sys
sync
games
man
lp
mail
news
uucp
proxy
www-data
backup
list
irc
gnats
nobody
libuuid
syslog
messagebus
landscape
sshd
pollinate
ubuntu
jenkins
```

## Jenkins cannot use `sudo`

If you get the following message

```
jenkins is not in the sudoers file.  This incident will be reported.
```

you may need to add the Jenkins user to the sudoers file (there may be another to install or use `sudo` under another user, but I am doing the following steps for convenience).

To add `jenkins` to the sudoers file you need to login as `root` and run the following command.

```
echo 'jenkins ALL=(ALL) ALL' >> /etc/sudoers
```

_Note: The user you are login as must be apart of the `admin` group on the Ubuntu in order to access the `root`, which `jenkins` is not._


## Unknown phantom SSH problem

_I am unsure as to what solved this issue, with Jenkins authenticating against GitHub for repository access, but it sort of resolved itself with one of the following options:_

- Using the `jenkins` user system credentials,
- Jumping between repositories within the job configuration screen,
- Updating the Git and Git Client Jenkins plugins

_Otherwise I am still unsure._


# References

[Install Jenkins on Ubuntu]: https://wiki.jenkins-ci.org/display/JENKINS/Installing+Jenkins+on+Ubuntu "Installing Jenkins on Ubuntu"

[Generating SSH Keys]: https://help.github.com/articles/generating-ssh-keys "Generating SSH Keys"

[Ubuntu Users]: http://askubuntu.com/questions/410244/a-command-to-list-all-users-and-how-to-add-delete-modify-users "A command to list all users? And how to add, delete, modify users?"

[Sudo Jenkins]: http://www.linuxforums.org/forum/red-hat-fedora-linux/144428-solved-not-sudoers-file-incident-will-reported.html "[SOLVED] is not in the sudoers file. This incident will be reported."
[Root user on Ubuntu]: http://superuser.com/questions/396540/i-created-an-ubuntu-server-do-i-have-a-root-user "I created an Ubuntu server. Do I have a “root” user?"

# Images

