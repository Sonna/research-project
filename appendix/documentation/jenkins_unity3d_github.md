# Jenkins setup for Unity3D & GitHub

1.  Navigate to the Jenkins homepage

    ![Jenkins index][]

2.  Create a new job.

3.  This new job will "Build a free-style software project".

    ![Jenkins - new job][]

4.


# Troubleshooting

It is impossible to activate Unity3D via the Commandline. It has to been opened at least once.

```
Unity has not been activated with a valid License. You must activate Unity with a valid license before using it. (also in batchmode)
```


# References

[Setting up Jenkins for Github and Xcode, with Nightlies]: http://orangejuiceliberationfront.com/setting-up-jenkins-for-github-and-xcode-with-nightlies/

[Unity3D Command Line Documentation]: http://docs.unity3d.com/Manual/CommandLineArguments.html "Unity3D Command Line Arguements"

# Images

[Jenkins index]: images/jenkins/screenshot_2014-09-01_5.49.38_pm.png "Jenkins Homepage"
[Jenkins - new job]: images/jenkins/screenshot_2014-09-01_5.57.35_pm.png "Jenkins - Create a new job"
