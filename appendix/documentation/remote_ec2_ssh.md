# Remote EC2 instance SSH access

**Prerequisites:**
- Programs:
  - On Windows you will need to use Putty (or possibly Cygwin)
  - _Mac OS X or Linux should have this by default_
- Have a copy of the created instance's SSH key; e.g. `~/.ssh/linux.pem`

1.  Open terminal and enter the following command

    ```
    ssh -i ~/.ec2/jenkins-linux.pem ubuntu@ec2-54-252-166-106.ap-southeast-2.compute.amazonaws.com
    ```

# Troubleshooting


# References

[SSH Linux EC2 instance]: http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html "Connecting to Your Linux/Unix Instances Using SSH"

# Images

