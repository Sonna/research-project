# Setting up SonarQube

[SonarQube][] is a an open source, browser based tool to manage code quality. As such, it covers the 7 axes of code quality:
1. Architecture & Design
2. Coding rules
3. Comments
4. Complexity
5. Duplications
6. Potential bugs
7. Unit tests

## How to install (Mac)

To install on OS X I recommend using Homebrew to install SonarQube.

1.  Ensure Homebrew is up to date and install SonarQube with the following commands.

    ```
brew update
brew install sonar
brew install sonar-runner
    ```

    ![Homebrew install sonar][]
    ![Homebrew install sonar-runner][]

2.  Prepare a database. _For this example I will be using PostgreSQL._

    Log into PostgreSQL and then create the Sonar user with database.

    ```
$ psql
psql (9.3.5)
Type "help" for help

sonna=# CREATE USER sonar WITH PASSWORD 'sonar';
CREATE ROLE

sonna=# CREATE DATABASE sonar WITH OWNER sonar ENCODING 'UTF8';
CREATE DATABASE
    ```

    ![Postgres create user and database][]

3.  Update SonarQube configuration to use PostgreSQL database.

    Edit the `sonar.properties` file in the _conf_ subdirectory to configure Sonar to utilize PostgreSQL instead of H2.

    ```
vi /usr/local/Cellar/sonar/4.4/libexec/conf/sonar.properties
    ```

    Then comment out the H2 reference the hash symbol `#` in front of the line:

    ```
sonar.jdbc.url: jdbc:h2:tcp://localhost:9092/sonar
    ```

    And uncomment the PostgreSQL reference by removing the hash symbol `#` in front of this line:

    ```
sonar.jdbc.url: jdbc:postgresql://localhost/sonar
    ```

    Then do the same changes to Sonar-Runner's configuration file as well.

    ```
vi /usr/local/Cellar/sonar-runner/2.1/libexec/conf/sonar.properties
    ```



# References

[SonarQube]: http://www.sonarqube.org/ "SonarQube"
[chapter31]: http://chapter31.com/2013/05/02/installing-sonar-source-on-mac-osx/ "Installing Sonar Source on Mac OSX"

# Images

[Homebrew install sonar]: images/sonarqube/screenshot_2014-09-10_3.05.41pm.png "Homebrew install sonar"
[Homebrew install sonar-runner]: images/sonarqube/screenshot_2014-09-10_3.04.06pm.png "Homebrew install sonar-runner"

[Postgres create user and database]: images/postgres/screenshot_2014-09-10_4.35.10pm.png "PostgreSQL create user and database"
