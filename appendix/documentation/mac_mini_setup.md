# Setting up a Mac Mini as a Continuous Integration server

1. Turn on and setup the mac mini basic settings

2. Enable screen sharing. This will allow the developer to remote in the mac mini without requiring to present in front of the device. [OS X Mavericks: Screen sharing][]

[Follow the instructions provided on Apple's website to setup screen sharing.][OS X Mavericks: Share your screen]

  > You can let others view your computer screen on their own Mac. While your screen is being shared, the user of the other Mac sees what’s on your screen and can open, move, and close files and windows, open apps, and even restart your Mac.
  >
  > 1. Open Sharing preferences if it isn’t already open (choose Apple menu > System Preferences, then click Sharing).
  > 2. Select the Screen Sharing checkbox.
  > If Remote Management is selected, you must deselect it before you can select Screen Sharing.
  >
  > 3. To specify who can share your screen, select one of the following:
  > | Option | Description |
  > ---
  > | **All users:** | Anyone with a user account on your Mac can share your screen. |
  > | **Only these users:** | Screen sharing is restricted to specific users. |
  >
  > 4. If you selected “Only these users,” click Add (+) at the bottom of the Users list, then do one of the following:
  > - Select a user from Users & Groups, which includes all the users of your Mac.
  >
  > - Select a user from Network Users or Network Groups, which includes everyone on your network.
  >
  > 5. To let others share your screen without having a user account on your Mac, click Computer Settings, then select one or both of the following:
  >
  > | Option | Description |
  > ---
  > | **Anyone may request permission to control screen:** | When other computer users begin screen sharing your Mac, they can ask for permission instead of entering a user name and password. |
  > | VNC viewers may control screen with password: | Other users can share your screen using a VNC viewer app—on iPad or a Windows PC, for example—by entering the password you specify here. |
  >
  > If this computer’s screen is shared only by other OS X users, turn off these options and add accounts for the other users.

  After this setup is complete you should be able to connect remotely, by following [Apple's instuctions][OS X Mavericks: Share your screen using Back to My Mac].

  > **Connect to your remote Mac**
  >
  > 1. To see your remote computers in the sidebar of a Finder window, choose Finder > Preferences, click Sidebar, then select Back To My Mac in the Shared section.
  > 2. Open a Finder window. If no shared computers appear in the Shared section of the sidebar, put the pointer on the word “Shared” and click Show.
  > 3. Click the remote Mac, then click Share Screen.
  >    If you don’t see the Mac you want, click All to see all the available computers, double-click the one whose screen you want to share, then click Share Screen.

3. Install a Continuous Integration service.

   For this step, I will install Jenkins, however I will be installing it via a package manager instead of with its default installer. This will minimize the amount of steps involved and will manage the setup of Mac OS X web server settings along with it.

   [![Install Homebrew][]][Homebrew]

   To install Homebrew you will need to run the following command.

   ```shell
   ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
   ```

   During the installation process Homebrew will prompt you for information; such as permissions. It will also install Xcode command line tools, which installs Git, a version control system, and other essential tools.

   Use Homebrew to install Jenkins.

   ```shell
   brew install jenkins
   ```

    After the installation of Jenkins has finished you will to start the service, which can be achieved with the following.

    ```shell
    java -jar /usr/local/opt/jenkins/libexec/jenkins.war
    ```

    However, you should ensure that Jenkins starts when the computer starts and you have logged in.

    ```shell
    ln -sfv /usr/local/opt/jenkins/*.plist ~/Library/LaunchAgents
    ```

    _With the symlink you can also start Jenkins with the following._

    ```shell
    launchctl load ~/Library/LaunchAgents/homebrew.mxcl.jenkins.plist
    ```

    _This will ask you to install Java, if it is not installed already._

    **Troubleshooting**

    Initially I was unable to access the Jenkins server dashboard remotely, on the same network. To fix this I had to update the IP address within the default Jenkins property list provided by Homebrew from `127.0.0.1` to `0.0.0.0`.

    _This solved with help from [SuperUser answer][[Jenkins not accessible through LAN]]._

    > The default homebrew.mxcl.jenkins.plist from homebrew has the following:
    >
    > ```<string>--httpListenAddress=127.0.0.1</string>```
    > This makes it listen only on localhost. To make it listen on all interfaces you can replace that with:
    >
    > ```<string>--httpListenAddress=0.0.0.0</string>```
    > Then I think you need to launchctl unload/load to restart. You could also put a more specific listen address if you want, of course.

4.  Install Unity3D.

    For this step I will use Homebrew Cask, which is a Homebrew extension that allows for the installation of familiar applications like Skype, Google Chrome or in this case Unity3D.

    _It also helps with managing versions and keeping applications up to date, like Homebrew._

    ![Install Homebrew Cask][]

    To install Homebrew Cask use the following command.

    ```
    brew install caskroom/cask/brew-cask
    ```

    Once Homebrew Cask has been installed, then install Unity3D with the following command.

    ```
    brew cask install unity3d
    ```

    - _Be aware it will take some time to download and install Unity3D._
    - _It will also ask for a password to install certain components._

5.

# References

[OS X Mavericks: Screen sharing]: http://support.apple.com/kb/PH14148
[OS X Mavericks: Share your screen]: http://support.apple.com/kb/PH14128?viewlocale=en_US
[OS X Mavericks: Share your screen using Back to My Mac]: http://support.apple.com/kb/PH14179?viewlocale=en_US
[Net Tuts+ - Screen Sharing on The Mac]: http://computers.tutsplus.com/tutorials/screen-sharing-on-the-mac--mac-30736
[NSScreencast - Setting up Jenkins]: http://nsscreencast.com/episodes/13-setting-up-jenkins

[Homebrew]: http://brew.sh/

[Jenkins homebrew server]: http://albertjwu.com/setting-up-home-network-installing-and-setting-up-jenkins/ "Setting up Home Network – Installing and setting up Jenkins"
[Jenkins not accessible through LAN]: http://superuser.com/questions/650420/jenkins-not-accessible-through-lan "Jenkins not accessible through LAN"

# Images

[Install Homebrew]: images/mac_mini_homebrew/Screenshot_2014-08-31_7.26.01_pm.png
[Install Homebrew Cask]: images/mac_mini_homebrew_cask/screenshot_2014-09-01_6.23.20_pm.png
[Install Homebrew Cask - install command]: images/mac_mini_homebrew_cask/screenshot_2014-09-01_6.26.44_pm.png


# Appendix

Example of the Homebrew package manager install script for Jenkins.
```ruby
require "formula"

class Jenkins < Formula
  homepage "http://jenkins-ci.org"
  url "http://mirrors.jenkins-ci.org/war/1.577/jenkins.war"
  sha1 "5385efc8c088aa868d73b6fd16fee8bc9845d39d"

  head "https://github.com/jenkinsci/jenkins.git"

  def install
    if build.head?
      system "mvn clean install -pl war -am -DskipTests"
      libexec.install "war/target/jenkins.war", "."
    else
      libexec.install "jenkins.war"
    end
  end

  plist_options :manual => "java -jar #{HOMEBREW_PREFIX}/opt/jenkins/libexec/jenkins.war"

  def plist; <<-EOS.undent
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
    <plist version="1.0">
      <dict>
        <key>Label</key>
        <string>#{plist_name}</string>
        <key>ProgramArguments</key>
        <array>
          <string>/usr/bin/java</string>
          <string>-Dmail.smtp.starttls.enable=true</string>
          <string>-jar</string>
          <string>#{opt_libexec}/jenkins.war</string>
          <string>--httpListenAddress=127.0.0.1</string>
          <string>--httpPort=8080</string>
        </array>
        <key>RunAtLoad</key>
        <true/>
      </dict>
    </plist>
  EOS
  end

  def caveats; <<-EOS.undent
    Note: When using launchctl the port will be 8080.
    EOS
  end
end
```

Example of the Homebrew Cask package manager install script for Unity3D.
```ruby
class Unity3d < Cask
  version '4.5.2f1'
  sha256 'a6330f8b42865636a8c2055b72fbdff5775b7019254ca41437eca0be1b9baedf'

  url 'http://netstorage.unity3d.com/unity/unity-4.5.2.dmg'
  homepage 'http://unity3d.com/unity/'

  install 'Unity.pkg'
  uninstall :pkgutil => 'com.unity3d.*'
end
```
