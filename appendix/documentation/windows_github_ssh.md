# Windows GitHub SSH access

_Unfortunately the following guide will require you to log into the remote machine via remote desktop client._

**Prerequisites:**
You should probably have `git` installed on the Windows computer if you are going to use GitHub (alternatively you could install GitHub's "GitHub for Windows" desktop client).

![No git][]

0.  Install Git with the following Chocolately command

    ```
$ choco install git
    ```

![Choco install][]

1.  Once Git has been installed the Jenkins slave node configuration will need to know where it is. Enter the following into a new "Tool Location" for Git

    ```
C:\Program Files (x86)\Git\bin\git.exe
    ```

![Windows Slave Node Git configure][]


# Troubleshooting


# References
[SSH keys for Windows]: https://help.github.com/articles/generating-ssh-keys#platform-windows "Generating SSH Keys for Windows"

[Windows slave configure]: http://stackoverflow.com/questions/8639501/jenkins-could-not-run-git "Jenkins could not run git"

# Images

[windows desktop image]: images/windows_github/screenshot_2014-09-22_11.40.34.png "Windows Desktop"
[No git]: images/windows_github/screenshot_2014-09-22_11.50.15.png "No Git, better install"
[Choco install]: images/windows_github/screenshot_2014-09-22_11.50.58.png "Installing Git via the Chocolately Gods"
[Git installed]: images/windows_github/screenshot_2014-09-22_11.54.18.png "Git was installed via the Chocolately Gods"
[Git powershell]: images/windows_github/screenshot_2014-09-22_11.58.14.png "Git in Powershell"

[Windows Slave Node Git configure]: images/windows_github/screenshot_2014-09-22_12.03.57.png "configure Git within Jenkins Windows slave"

[Unity Console trouble]: images/windows_github/screenshot_2014-09-22_12.09.45.png "Cannot find Unity via command line"
