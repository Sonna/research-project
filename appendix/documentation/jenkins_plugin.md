# Jenkins plugin setup

1.  Open Jenkins "Manage Jenkins" page

    [![Jenkins - Manage]][Jenkins/Manage]

2.  Go to "Manage Plugins"

    [![Jenkins - Manage Plugins]][Jenkins/PluginManager]

3.  You need to search for and install the following Jenkin plugins:
    - Git
      ![Jenkins - Install the Git plugin][]
    - Unity3D
      ![Jenkins - Install the Unity3D builder plugin][]
      _Assuming you already have Unity3D installed._
    - GitHub
      ![Jenkins - Install the GitHub plugin][]

    Once all plugins have been selected "Download now and install after restart"

4.


# References

[Jenkins/Manage]: http://localhost:8080/manage
[Jenkins/PluginManager]: http://localhost:8080/pluginManager/
[Jenkins Slack Notifier]: https://jenkins.ci.cloudbees.com/job/plugins/job/slack-plugin/20/org.jenkins-ci.plugins$slack/

# Images

[Jenkins - Manage]: images/jenkins/screenshot_2014-09-01_6.03.55_pm.png "Jenkins - Manage Jenkins page"
[Jenkins - Manage Plugins]: images/jenkins/screenshot_2014-09-01_6.12.17_pm.png "Jenkins - Manage Jenkins Plugins page"
[Jenkins - Install new plugins]: images/jenkins/screenshot_2014-09-01_8.47.29_pm.png "Manage Jenkins Available Plugins"
[Jenkins - Install the Git plugin]: images/jenkins/screenshot_2014-09-01_8.47.42_pm.png "Find and Install the Git Jenkins Plugin"
[Jenkins - Install the Unity3D builder plugin]: images/jenkins/screenshot_2014-09-01_8.51.34_pm.png "Find and Install the Unity3D builder Jenkins Plugin"
[Jenkins - Install the GitHub plugin]: images/jenkins/screenshot_2014-09-01_9.00.23_pm.png "Find and Install the GitHub Jenkins Plugin"
