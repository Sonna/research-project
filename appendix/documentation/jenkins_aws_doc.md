# Setting up Jenkins in the Amazon Cloud

_Please note that this guide will be following the majority of instructions
present in the [McKee][] and [Waddington][] articles on setting up Jenkins
in an Amazon ECS instance._

## Amazon Web Services Console

![Amazon Web Services Console homepage][fig1]

Once you have logged in ensure that you set region settings in the top-right so that it using the closet Amazon server location.

This is to ensure that the following server instance that the user is about to create are within the country they are located so that local laws apply, but also means that will hopefully be less lag between them and the server.[^Why should I care where my AWS server is located?]

![AWS Console - Region Settings][fig2]
_In my case that would "Asia Pacific (Sydney)"_


## Creating an Amazon EC2 Linux (Ubuntu) Instance

After signing up to Amazon Web Services and logging into the AWS Console, head over to the [EC2 Console][]

![AWS EC2 Console][fig3]

_The AWS EC2 Console will look something like the following._

![AWS EC2 Console landing page][fig4]

On the EC2 Console page select _"Launch instance"_ and begin the create instance wizard.

![AWS EC2 Console landing page][fig5]


### Create Instance

1.  The first within the wizard will be to select an Operating System image.

    ![Create Instance - Step 1][fig6.1.1]

    Because this documentation is following the tutorial present in [Waddington's article][Waddington], we will chose the "Ubuntu Server 14.04 LTS", which is _Free tier eligible_, and leave it as 64-bit.

    ![Create an Ubuntu Server 14.04 Instance][fig6.1.2]

    **AWS-CLI Alternative**

    Alternatively you could use AWS-CLI ``` aws ec2 run-instances ``` and pass it the ``` --image-id ``` option when creating a new instance; e.g.

    ```
    aws ec2 run-instances \
      --image-id ami-fddabdc7
    ```

2.  After selecting for AMI it will take you to the following screen and ask you to choose a combination of CPU, memory, storage and networking capacity. For this documentation the **t2.micro** will be enough.

    ![Choose an Instance Type][fig6.2]

    **AWS-CLI Alternative**

    Alternatively you could use AWS-CLI ``` aws ec2 run-instances ``` and pass it the ``` --instance-type ``` option when creating a new instance; e.g. continuing the above example

    ```
    aws ec2 run-instances \
      --image-id ami-fddabdc7 \
      --instance-type t2.micro
    ```

3.  Leave as defaults and continue.

    ![Choose an Instance Type][fig6.3]


...

Alternatively, instead going through the webpages and prompt you could install the [AWS Command Line Interface][AWS CLI].[^Install AWS-CLI (Windows vs. Mac)]

```
aws ec2 run-instances --image-id ami-xxxxxxxx --count 1 --instance-type t2.micro --key-name MyKeyPair --security-groups MySecurityGroup
```
_Example of creating a micro EC2 instance_

---

# Footnotes

[^Why should I care where my AWS server is located?]: Put simply, the location of the server means what country laws apply to it and what laws you must abide by; e.g. You maybe legally required to mantain and ensure that any Australian contact information remains within Australia.

[^Install AWS-CLI (Windows vs. Mac)]: On Windows you have to install it via a installer package, whereas on Mac and Linux it can be install via **pip**.

# Links

[AWS]: http://aws.amazon.com
[AWS CLI]: http://aws.amazon.com/cli/ "AWS Command Line Interface"
[aws-cli repo]: https://github.com/aws/aws-cli "AWS-CLI GitHub Repository"
[EC2 Console]: https://console.aws.amazon.com/ec2/
[pip]: https://pip.pypa.io/en/latest/

# References

[McKee]: http://aptobits.com/2012/09/setting-up-jenkins-ci-on-an-amazon-ec2-linux-instance/
[Waddington]: http://www.tristanwaddington.com/2012/03/installing-jenkins-on-an-ubuntu-amazon-ec2-instance/

[aws ec2 run-instances]: http://docs.aws.amazon.com/cli/latest/reference/ec2/run-instances.html

# Images

[fig1]: images/amazon_console/Screenshot_2014-08-17_12.34.31.png "AWS Console landing page"
[fig2]: images/amazon_console/Screenshot_2014-08-17_15.18.49.png "AWS Console - Region Settings"
[fig3]: images/amazon_ec2_console/Screenshot_2014-08-17_16.50.04_highlight.png "AWS Console - AWS EC2 Console"
[fig4]: images/amazon_ec2_console/Screenshot_2014-08-17_16.56.03.png "AWS EC2 Console landing page"
[fig5]: images/amazon_ec2_console/Screenshot_2014-08-17_16.56.03_highlight.png "AWS EC2 Console - Create Instance"

[fig6.1.1]: images/amazon_ec2_console/create_instance_wizard/Screenshot_2014-08-17_17.03.55.png "Create Instance Wizard - Step 1: Choose an Amazon Machine Image (AMI)"
[fig6.1.2]: images/amazon_ec2_console/create_instance_wizard/Screenshot_2014-08-17_17.03.55_highlight.png "Create Instance Wizard - Step 1: Choose Ubuntu Server 14.04 LTS"

[fig6.2]: images/amazon_ec2_console/create_instance_wizard/Screenshot_2014-08-17_19.07.05.png "Create Instance Wizard - Step 2: Choose an Instance Type"

[fig6.3]: images/amazon_ec2_console/create_instance_wizard/Screenshot_2014-08-17_19.25.16.png "Create Instance Wizard - Step 3: Configure Instance Details"

# Abbreviations

*[AWS]: Amazon Web Services
