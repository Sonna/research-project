# SonarQube and Jenkins

1.  First install both Jenkins and SonarQube

2.  Install the Sonar plugin within Jenkins.

    ![install sonar plugin within jenkins][]

3.  Configure Jenkins global system settings for SonarQube.

    ![configure jenkins sonar][]
    ![configure jenkins sonar-runner][]


# References

[SonarQube]: http://www.sonarqube.org/ "SonarQube"

[SonarQube Jenkins Plugin]: http://docs.codehaus.org/display/SONAR/Configuring+SonarQube+Jenkins+Plugin "Configuring SonarQube Jenkins Plugin"
[SonarQube plugin]: https://wiki.jenkins-ci.org/display/JENKINS/SonarQube+plugin "SonarQube plugin"

[Petthawadu 2014]: http://lasithapetthawadu.wordpress.com/2014/05/03/configure-jenkins-with-sonarqube-for-static-code-analysis-and-integration/ "Continuous integration and static code analysis"

# Images

[install sonar plugin within jenkins]: images/jenkins_sonarqube/screenshot_2014-09-10_5.01.32pm.png "SonarQube Jenkins plugin"
[configure jenkins sonar]: images/jenkins_sonarqube/screenshot_2014-09-10_5.27.26pm.png "Configure Jenkins Sonar"
[configure jenkins sonar-runner]: images/jenkins_sonarqube/screenshot_2014-09-10_5.11.36pm.png "Configure Jenkins Sonar-Runner"
