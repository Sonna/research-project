# SonarQube C# project

1.  Install [C# plugin][Sonar C# plugin] within SonarQube.

    Login. Navigate to the SonarQube [Update Center][];
    e.g. `http://localhost:9000/updatecenter`

    ![Install C# plugin][]




# References

[SonarQube]: http://www.sonarqube.org/ "SonarQube"

[Wright, J 2011]: http://www.wrightfully.com/setting-up-sonar-analysis-for-c-projects/ "Setting up SonarQube analysis for C# projects"
[Sonar C# plugin]: http://docs.codehaus.org/display/SONAR/C%23+Plugin "C# Plugin"
[Update Center]: http://localhost:9000/updatecenter


# Images

[Install C# plugin]: images/sonarqube/screenshot_2014-09-10_5.43.59pm.png "SonarQube C# plugin"
