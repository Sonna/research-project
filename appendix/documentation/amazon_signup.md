# Signup to Amazon Webservices

1.  To begin signing up for Amazon Web Services, goto the [AWS homepage][AWS] and select the _"Create a Free Account"_ button.

    ![Amazon Web Services homepage][fig1]

2.  Enter your e-mail address[^Gmail Alias], and select _"I am a new user"_ to continue.

    ![Signin Page][fig2]

3.  This will take you Amazon's additional _"Login page"_ to finialise your login details; i.e. Name and password

    ![Login Page][fig3]

4. It will then take you to a _"Contact Information Form"_, which you will need to provide your address details, answer a CAPTCHA and agree to their terms & conditions.

    ![Contact Information Form][fig4]

5.  It will then take you to a _"Payment Information Form"_, which needs to be filled out. Whilst they do provide a Free Tier for a small usage of their services should you exceed those limits, like _750hrs/month_ computations, you will be charged.

    ![Payment Information Form][fig5]

6. After providing contact and payment information it will require you verify youself via a phone call. During the phone call the ask for the on-screen PIN via the phone's keypad.

    ![Identity Verification Form - Part 1][fig6.1]
    ![Identity Verification Form - Part 2][fig6.2]
    ![Identity Verification Form - Part 3][fig6.3]

7. After you have verified yourself you have the option to select a _"Support Plan"_. Otherwise let as a the default _"Basic (Free)"_ and continue.

    ![Support Plan][fig7]

8. Then you should be finished and can now access the AWS console.

    ![Finished creating an account][fig8]

---

<!-- # Footnotes -->

[^Gmail Alias]: I am using a personal aacount. However the ``` +AWS ``` Gmail alias is being used to separate it from other emails, for sorting purposes.


<!-- # Links -->

[AWS]: http://aws.amazon.com


<!-- # References -->


<!-- # Images -->

[fig1]: images/amazon_signup/Screenshot_2014-08-17_13.31.07.png "AWS Homepage"
[fig2]: images/amazon_signup/Screenshot_2014-08-17_13.46.13.png "AWS Create Account - Signin Page"
[fig3]: images/amazon_signup/Screenshot_2014-08-17_13.57.55.png "AWS Create Account - Login Page"
[fig4]: images/amazon_signup/Screenshot_2014-08-17_13.58.43.png "AWS Create Account - Contact Information Form"
[fig5]: images/amazon_signup/Screenshot_2014-08-17_14.00.44.png "AWS Create Account - Payment Information Form"
[fig6.1]: images/amazon_signup/Screenshot_2014-08-17_14.23.09.png "AWS Create Account - Identity Verification Form - Part 1"
[fig6.2]: images/amazon_signup/Screenshot_2014-08-17_14.50.05.png "AWS Create Account - Identity Verification Form - Part 2"
[fig6.3]: images/amazon_signup/Screenshot_2014-08-17_14.50.51.png "AWS Create Account - Identity Verification Form - Part 3"
[fig7]: images/amazon_signup/Screenshot_2014-08-17_14.51.11.png "AWS Create Account - Support Plan"
[fig8]: images/amazon_signup/Screenshot_2014-08-17_14.51.38.png "AWS Create Account - Finish"


<!-- # Abbreviations -->

*[AWS]: Amazon Web Services
*[CAPTCHA]: Completely Automated Public Turing test to tell Computers and Humans Apart
*[PIN]: Personal Identification Number
