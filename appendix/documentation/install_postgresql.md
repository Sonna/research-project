# Installing PostgreSQL

[PostgreSQL][] is a powerful, open source object-relational database system.

## How to install (Mac)

To install on OS X I recommend using Homebrew to install PostgreSQL.

1.  Ensure Homebrew is up to date and install PostgreSQL with the following commands.

    ```
brew update
brew install postgres
    ```

    ![homebrew install postgres - part 1][]
    ![homebrew install postgres - part 2][]
    ![homebrew install postgres - part 3][]

2.  Setup PostgreSQL to start at login.

    First check if LaunchAgents Directory exists, otherwise make it.

    ```
mkdir -p ~/Library/LaunchAgents
    ```

    To have launchd start postgresql at login:

    ```
ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents
    ```

3.  Then start PostgreSQL now.

    ```
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
    ```

    ![homebrew install postgres - part 4][]

4.  Update the PostgreSQL path for OS X.

    Because Mac OS X comes with PostgreSQL pre-installed the path to which binary is used needs to be updated to get Homebrew's installed Postgres instead of OS X's.

    ```
sudo vi /etc.paths
    ```

    _Or use whatever terminal text editor you need to edit the paths file._

    Then move the last line that points to Homebrew's binaries and move it to the top, so that it is used first.

    **Before**

    ![paths file - before][]

    Use the following command within Vim to move the line to the top of the paths file.

    ```
:m0
    ```

    ![paths file - move last line][]

    **After**

    ![paths file - after][]

5.  Initialize the database cluster.

    ```
initdb /usr/local/var/postgres -E utf8
    ```

    ![initdb][]

6.  Create a role.

    ```
createuser -d -P postgres
    ```

    ![create postgres user][]

Now you can login to PostgreSQL from Terminal with

```
$ psql -U postgres
psql (9.3.3)
Type "help" for help.

postgres=>
```

# References

[PostgreSQL]: http://www.postgresql.org/ "PostgreSQL"

[CodeFellows]: https://www.codefellows.org/blog/three-battle-tested-ways-to-install-postgresql "Three Battle Tested Ways to Install PostgreSQL"
[Belyamani, M 2012]: http://www.moncefbelyamani.com/how-to-install-postgresql-on-a-mac-with-homebrew-and-lunchy/ "How to Install PostgreSQL on a Mac With Homebrew and Lunchy"
[MARCINKUBALA 2013]: http://marcinkubala.wordpress.com/2013/11/11/postgresql-on-os-x-mavericks/ "POSTGRESQL ON OS X MAVERICKS"

# Images

[homebrew install postgres - part 1]: images/postgres/screenshot_2014-09-10_3.24.15pm.png
[homebrew install postgres - part 2]: images/postgres/screenshot_2014-09-10_3.24.24pm.png
[homebrew install postgres - part 3]: images/postgres/screenshot_2014-09-10_3.26.57pm.png
[homebrew install postgres - part 4]: images/postgres/screenshot_2014-09-10_3.43.12pm.png

[paths file - before]: images/postgres/screenshot_2014-09-10_3.53.14pm.png "Before"
[paths file - move last line]: images/postgres/screenshot_2014-09-10_3.53.28pm.png "Vi move line command"
[paths file - before]: images/postgres/screenshot_2014-09-10_3.53.39pm.png "After"

[initdb]: images/postgres/screenshot_2014-09-10_4.02.57_pm.png "Initialize PostgreSQL database"
[create postgres user]: images/postgres/screenshot_2014-09-10_4.17.28pm.png "Create PostgreSQL user"
