---
1. Write one sentence about the main problem addressed in the paper.

2. Write one sentence why the problem stated is a real problem; motivate why
   dealing with the problem is of interest.

3. Summarise your approach to solve/address the problem in one sentence.

4. Finally, give evidence (one to two sentences) why your approach solves /
   addresses the problem
---


# Potential Abstracts

Domain:
- Continuous Integration Environment for Unity3D environments for coders &
  designers for developing

Question:
- How can & be used (spec/reg)
- How is it used? (Pro's/Con/s margin)

Method:
- Observe team using a CIE
- maybe others?

Outcomes?
- Better understanding of how affects team workflow/team management include team
  culture with respect to process
- Ideally identifies benefits & penalties in the use of such a CIE
- Guidelines


## Abstract #1
_Unity3D is development tool for creating 2D & 3D games and is becoming
increasing popular with independent developers. ... ._

> component-oriented system

> Continuous Integration is a software development practice where members of a
> team integrate their work frequently, usually each person integrates at least
> daily - leading to multiple integrations per day.

## Abstract #2
_Continuous Integration is becoming increasingly common in software development
industry; that is to code, test, and integrate in an iterative manner. This
study will consider how undergraduate software engineering students utilise a
continuous integration system for Unit3D development and observe its effect on
student participation and performance. The approach will assess each students
project management experience, and through observations and reflective analysis
consider how the students utilise and relate to this type of practice. It is
expected that clear relationship between individual experience and adoption of
the practice will be observed, and it is also hoped that guidelines to help new
developers can be determined._

> completing their final year projects that were given a suite of software
> development tools with continuous integration that imitated a typical
> industrial environment. Assessment was made of students’ project management
> experience rather than of project deliverables, using anecdotal evidence to
> measures how it affect  students’ participation and performance._

> Future work will investigate automation of the assessment of continuous
> integration and configuration management server data.

## Abstract #2.5 - Proposal to do the "exploration"
_Software applications are not just stand-alone systems anymore. They have
become a suite of interconnect heterogeneous components that execute or
encapsulate data, whilst being dispersed over a computer network. This often
makes it difficult for developers to integrate new features and maintain old
ones. A cause of this is that any changes to an existing code base are often met
with resistance, out of possible fears of breaking the application. This work
will explore the process of "Continous Integration" practice, and proposed its
benefits over traditional development methods, within current literature using
a structured approach._

## Abstract #3 - Paper of Work ("explore" lit. rev.) that has been Done
_Software applications are not just stand-alone systems anymore. They have
become a suite of interconnect heterogeneous components that execute or
encapsulate data, whilst being dispersed over a computer network. This often
makes it difficult for developers to integrate new features and maintain old
ones. A cause of this is that any changes to an existing code base are often met
with resistance, out of possible fears of breaking the application. This paper
explores the process of "Continous Integration" practice and its benefits over
traditional development methods._
