# Methods / Techniques

We need methods of data collection _(not analysis)_ and analysis methods for topics such as:
- Individual activity _(methods not results, heat maps etc)_
- Repository data _(team level - about the code. inc. videos)_
- Team culture / values _(data only, not opinion if possible)_
