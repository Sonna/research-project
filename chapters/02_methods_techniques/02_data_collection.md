# Methods of data collection

**Name**: Alex Sonneveld


## Context:

Outline the reason and context for the spike.

Example: Team needs to use a database to store data for application
developed during this subject and must learn to use the Firebird DBMS
tools to be able to script the creation and population of the database.

In order for a team of developers to gain a better insight of their progress and
successful processes within a project, they first need to know what information
can be collected and what methods / tools can be used to collect it.

Knowledge Gap
:   Learn about data collection methods / tools

Skill Gap
:   Is able to use data collection methods / tools effectively

Technology Gap
:   Data collection tools

_Provide details of the appropriate gaps related to this spike._

Example: The team is not familiar with the Firebird DBMS that has been
chosen by the subject conveners. Need to install and make use of the
following tools for Firebird:

-   FSQL command line tool
-   FlameRobin GUI admin tool


## Goals/Deliverables:

What are the goals and deliverables of this spike? Example:

The goal is to collect as much information from the available sources,
such as:

-   Scripts need to create database that models the application domain
-   Scripts to insert initial test data into the database
-   Batch file, makefile or MSBuild script that includes database setup
    and tear down

So that they then can be further analysis and discussed.


## Planning notes:

In order to collect data on developer projects, first the tools currently being
used internally should be investigated and analysis. Then other similar tools
should be investigate and compare to the current tools, listing their advantages
and disadvantages for data collection.

- Investigate project source control systems (SCM) or revision control systems;
  e.g.
    - [Git][],
    - [Mercurial][],
    - [Bazaar][],
    - [Subversion][],
    - [BitKeeper][],
    - etc.


<!--- # References -->

[Git]:        http://git-scm.com/             "Git"
[Mercurial]:  http://mercurial.selenic.com/   "Mercurial"
[Bazaar]:     http://bazaar.canonical.com/en/ "Bazaar"
[Subversion]: https://subversion.apache.org/  "Subversion"
[BitKeeper]:  http://www.bitkeeper.com/       "BitKeeper"
