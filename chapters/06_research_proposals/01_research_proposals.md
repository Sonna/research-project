# Research Proposals

The following is a list of research proposals for future investigate of ideas
that became apparent whilst researching during this project, but do not enough
time to pursue or are beyond scope.

## Time away and/or project complexity decrease overall activity

Going into second semester, a large majority of students spent less time
directly on the project overall. This could have been because the project was
complex and coming back into it cold for the second semster meant most
students had difficulty working long periods of time on it. Or, might mean
that project had acheived a higher abstraction through refactoring complexity
meaning less time was spent to acheive the same quality or amount of work.

In order to possibly measure these kinds of results it would need:

 1. The list worklog tasks completed within the first six week time period of
    second semester,

 2. The git logs of each project repository to find:

     - An estimate amount of time working on the project,

     - An estimate amount of effort working on the project; where work was
       happening and how much,

     - The complexity of the commits involved during this time and the time
       between commits (to see if it had changed between first and second
       semester),

     - etc.

 3. Review the worklogs and assessment criteria to see requirements or
    proirities had changed,


_Also, even with the above statistics, there might not be anything to find (
which is a result regardless, but methods of investigating and applying would
be useful for future anaylses)._


## Compare effort, complexity and lines of code

_This primarily an excuse to generate a three-way relation graph (i.e. a bubble
chart)._

For this research proposal, it is trying find if their is a direct coloration
between the complexity of "file" (class or script), the size of that script and
the amount effort involved with maintaining or improving it.

For this it would measure:

1. Effort; which could the amount of line added, deletion and modified or the
general amount of commits done per file,

2. Complexity; this would most likely be cyclomatic complexity, which would
measure how hard it would be for a person to understand said code due to the
operations it performs,

3. Size / Lines of code; this is to measure amount of code that is involved and
whether that contributes to the difficult of understanding or maintaining it


## Team collaboration increase effort and/or outcome quality

The worklogs record when team members are working and from this it can be
determine if they working together or alone. This information combined with the
revision history of project files could be use to track either an increase or
decrease in student effort, project outcomes quantities and qualities.

Also, this could be used determine whether or not certain people work better
as a team or individually, along if and how those behaviours/attributes change
over time; e.g. a meeting once a week may help a person work more effectively
inidividually or have a negative impact on their overall performance.

_Other possible events could also occur; like a person may struggle on a task
and being together as team may help resolve the issue, whereas them being
alone could halt progress all together._


## Changes in Code Quality throughout a day

This research proposal idea is based of what Rajesh Vasa has mentioned during
his lectures for Software Deployment and Evolution; "each and every decision is
made with a decision frame", which can be influenced by numerous factors
(including time of day). Some tasks require higher or more focused attention to
resolve and are hard sustain for long periods (causing _ego depletion_).

This _ego depletion_ is most likely to have a greater effect on a developer the
later into day they work (assuming they were awake or already working ahead
time). In order to measure if this is true, we would need to measure:

  1. When commits are being made; e.g. early in the morning, in the afternoon
     or during the night,

     _An example of this are the punch-card graphs seen on GitHub Git
     repositories, which could either be reproduced locally or rely on GitHub's
     API to collect this information from a given repository._

  2. The perceivable quality of software being produce; e.g cyclomatic
     complexity, adherence to code standards and/or test coverage, etc.

     _An example of a tool that could possibly track this information is
     SonarQube, which analysis static code for Software Quality (as a software
     service)._

With those, or similar values, being measured they then could be compared to
determine whether that hypothesis holds and developers do indeed performance
worse and/or produce less quality code the more later into day work is
committed.

_Can also analysis the length / period of time spent on single task, both in the
work-logs and the commits within the project's Git repository._


## Word Frequencies between Work-logs and project tools

Generate word frequency tables from the various tools and records kept
throughout the project; such as work-logs, Git repositories, project management
tools (GitHub and Trello), survey responses, etc.

By capturing this information it be compared and analysis between the different
sources for common thematic links or if the developer experiences are
consistent.

Other ideas / notes:

- _Collect a word frequency of the first word for each commit._


## Developers are more invested in process than the product

The idea for this research proposal comes from the initial word frequency clouds
produced from the Git repository commit messages. By analysing the frequency of
words from a Project's process and tools, we can find either a vague ideas about
the project, the project's process or progress.

_Should also look into measuring word frequencies within project management
tools; i.e. GitHub, Trello, etc._

_The biggest issue with this research proposal is that it is biased by the
assessment requirements for the project's Unit, which primarily focused on
software quality and process._

_We could also analysis the language used; e.g. emotive, calm , etc._


## Analysis if developers adhere to their own SQAP documentation

Analysis if developers are following code standards and practices by comparing
the provided Software Quality Assurance Plan (SQAP) documentation to source code
and commit messages within each Team project's Git repository.

Analysis if they are following:

1. Source Control standards; e.g. proper
   branch naming, use and consistent Git commit message between team members,
   etc,

2. Code standards; e.g. variable / method casing, interface prefixes, spaces vs
   tabs, etc,

3. Code practices; e.g. curly bracing on newlines, generics, defensive
   programming, etc,


## Gource GitHub issues

Gource (a Git visualization tool) has support for generating videos for custom
logs and not just for Git Repositories. Because of this it could feasible to use
the GitHub API to convert / transpose the tracking of opening and closing
issues by:

  - Sorting issues between folders that represent their open and closed state,
    or alternatively

  - Adding and removing open issues to represent the speed at which issues are
    being opened and resolved over time (this data could also be used to
    represent a moving burn-down chart to represent progress as well)

However, this visualisation would only be as effective as those who have used it
appropriately and may strong outliers to represent those who have misused or
used the tool strangely.

Also, I imagine the same process could be applied similarly to Trello via its
API and task moving between lists. However, again how it is interpreted relies
heavily on personal knowledge on how each of tools were used (and if used
appropriately).

_An example of a Gource log:_
```
1394957080|Alex Sonneveld|A|/Assets/Scripts/CameraController.cs.meta
1394957080|Alex Sonneveld|D|/Assets/Scripts/CameraController.cs.cs.meta
1394957338|Alex Sonneveld|M|/Assets/Scripts/CameraController.cs
```


<!-- Random thesis idea "What is a commit worth?" (joke line "about $40/hr") -->
