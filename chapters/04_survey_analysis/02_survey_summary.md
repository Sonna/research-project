# Survey summary

_Include the following details:_
- Summary,
- Data; i.e.
    + Charts/Matrix,
    + list of qualities,
    + etc

_May need to define terms & categories. These terms & categories are to describe the team members and behaviors, but need to defined so that can be reflected on in the later analysis, whilst addressing the same vocabulary._

> _Think about the traits exhibited by the developers, how the
> relate to one another, overall as a group and between groups._


