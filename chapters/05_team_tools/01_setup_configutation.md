# Setup configuration

_This section describes the overall Continuous Integration setup._

## Jenkins server

[Jenkins](http://jenkins-ci.org/) is "an extendable open source continuous integration server". (Jenkins 2014)

### What is Jenkins?

> Jenkins is an award-winning application that monitors executions of repeated jobs, such as building a software project or jobs run by cron. Among those things, current Jenkins focuses on the following two jobs:
>
> - **Building/testing software projects continuously**, just like CruiseControl or DamageControl. In a nutshell, Jenkins provides an easy-to-use so-called continuous integration system, making it easier for developers to integrate changes to the project, and making it easier for users to obtain a fresh build. The automated, continuous build increases the productivity.
> - **Monitoring executions of externally-run jobs**, such as cron jobs and procmail jobs, even those that are run on a remote machine. For example, with cron, all you receive is regular e-mails that capture the output, and it is up to you to look at them diligently and notice when it broke. Jenkins keeps those outputs and makes it easy for you to notice when something is wrong.
>
> ...
>
> "The ideas behind Continuous Integration have changed how companies look at Build Management, Release Management, Deployment Automation, and Test Orchestration."
>
> <cite>https://wiki.jenkins-ci.org/display/JENKINS/Meet+Jenkins</cite>

Jenkins is a web-based Java application that executes the test, build and deploy process involved with software projects. It main purpose is to help developers automated those process and assist with the integration of new features or fixing of bugs within the life-cycle of a software project.

Jenkins is a software as a service that implements the Continuous Integration (CI) software development practice.

> "[CI] is a software development practice" (Fowler, M 2006), which requires team members to frequently checking in and testing changes. Each integration with the main project base triggers automated tests and build procedures. This process detects errors as soon as possible to ensure that small bugs are caught and the application can be built early on. This approach often leads to significantly reduced integration problems and helps teams to quickly develop cohesive software.

### Jenkins Windows Setup

The first initial attempt to use Jenkins was setup on a Windows Server 2012 R2 virtual machine, within a VMWare ESXi 5.5 Hypervisor on an old home computer. This first attempt was a prove of concept to see if it could setup and could be setup on a operating system that runs the Unity3D game engine.

The reason for using Windows Server 2012 R2 as the operating system was because Unity3D (at the time of writing) will only run on either Windows or OS X and because it is easier/legal to virtualise Windows than OS X. This virtualisation would also, potentially, allow for the dumping of a virtual machine image between computers or on the computer as a backup.

_This computer was later retired for personal reasons._

Jenkins was setup using the default Windows installer that provided on the front page of its website. However, it was installed as a Windows service, which required additional (somewhat strange) configurations in order for it to communicate with and exchange SSH keys with GitHub (where the project repositories are hosted).

### Jenkins Mac-Mini Setup

The second attempt to setup a Jenkins server was on a Mac Mini. The main reasons for this was because it would easy to install, manage and configure web based applications or services within an Unix like environment; such as OS X. The second reason was because OS X is capable of running Unity3D and could also build iPhone or Mac applications, which can only be done on OS X.

This setup used the Homebrew package manager; an open-sourced and community driven package manager for OS X.


### Jenkins Amazon Setup

_This was meant to be the first second attempt. However, there were concerns about whether or not it would work when using Windows in the cloud and if it would communicate to another server or would it host its own website?_



## Survey

The "Games Lab" survey being used for collecting data from the team members about their developer (or designer) experiences with various tools and programs, in particular "Continuous Integration".

It is broken up into three sections:
1. Developer knowledge
2. Project Team Process
3. Continuous Integration

With each of the sections asking about their current experiences before introducing them to using Jenkins, a Continuous Integration tool.

This survey was built using:
- [Ruby on Rails](http://rubyonrails.org/); a web framework written in Ruby,
- [Surveyor Ruby Gem](https://github.com/NUBIC/surveyor), and
- [Heroku](https://heroku.com/); a simplified web deployment platform

The reason for using these tools was because of my personal expertises and familiarity with those tools. Whilst using the Surveyor Gem was new, its simple Domain Specific Language was simple to use and to quickly implement a web-based survey, with complex matrix grids.

_The activate survey could be reach via the following link:_
[vast-oasis-6543.herokuapp.com/surveys](vast-oasis-6543.herokuapp.com/surveys)

_A copy of this survey application can be found here:_
[BitBucket "Sonna / GamesLab_Suvery" Repository - https://bitbucket.org/Sonna/gameslab_suvery](https://bitbucket.org/Sonna/gameslab_suvery)


<!-- # References -->

Fowler, M 2006, 'Continuous Integration' <http://martinfowler.com/articles/continuousIntegration.html> [Accessed 23 August 2014]
