% An analysis of students software team development & activities
% Alex Sonneveld 7193475
% July 1, 2014

---
Title: An analysis of students software team dvelopment & activities
Author: Alex Sonneveld
fontsize: 11pt
documentclass: report
geometry:
  - margin=1in,
  - a4paper
  - width=150mm
  - top=25mm
  - bottom=25mm
  - bindingoffset=6mm
bibliography: chapters/bibliography.yaml
classoption: twoside, openright, titlepage
csl: csl/harvard-swinburne-university-of-technology.csl
lof: true
lot: true
abstract: |
  Continuous Integration is becoming increasingly common in software
  development industry; that is to code, test, and integrate in an iterative
  manner. This study will consider how undergraduate software engineering
  students utilise a continuous integration system for Unit3D development and
  observe its effect on student participation and performance. The approach
  will assess each students project management experience, and through
  observations and reflective analysis consider how the students utilise and
  relate to this type of practice. It is expected that clear relationship
  between individual experience and adoption of the practice will be observed,
  and it is also hoped that guidelines to help new developers can be
  determined.
...

<!-- \part{Report} -->

\begin{figure}[!h]
  \centering
  \begin{tikzpicture}[scale=1.25]
    \foreach
      \n\x\y in {1/0/0,2/0.25/-0.25,3/0.5/-0.5}
        \coordinate (\n) at (\x,\y);
    \foreach
      \k in {1,...,3}
        \pgfmathparse{12*\k}
        \draw[fill=blue!\pgfmathresult] (\k) circle (3.6-0.8*\k);
    \foreach
      \k/\text in {
        0/Real World,
        1/Worklogs,
        2/Repo log / data
      } \draw[
        decoration={
          text along path,
          reverse path,
          text align={align=center},
          text={\text}
        },
        decorate
      ] (2.9-0.8*\k+0.25*\k,-0.25*\k) arc (0:180:2.9-0.8*\k);
  \end{tikzpicture}
\end{figure}
