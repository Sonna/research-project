# Overall report structure


Small Docs that Capture Your Work

(Spike Plans + Outcomes ~ Reports/data/charts)

## [methods / technique]
- [ ] methods of data collection (not analysis)
- [ ] analysis methods for individual activity (methods not results, heat maps etc)
- [ ] analysis methods for repository data (team level - about the code. inc. videos)
- [ ] measure for team culture / values (data only, not opinion if possible)

##[looking at results from the actual teams or individuals]
- [ ] look at each team. aggregate. show charts, describe process/culture/outcomes/division of work etc
- [ ] do a report on each person. Show their charts, note features, heatmaps etc

##[programmer student surveys - what team members said/thought]
- [ ] matching qualities from survey results with other data
- [ ] summary of overall data from surveys
- [ ] thematic analysis (are there common ideas/thinking in the comments)

##[description of tools to help team practice]
- [ ] document spike tools
- [ ] document task/commit/merge stuff
- [ ] ... jenkins stuff

##[HD - detailed research proposals! Several]
* aim for three - try to show off what you know / understand / extrapolate beyond


-----

# Presentations
- [ ] 15 mins (beyond bullets points, see swinbrain on presentations)

- [ ] ACT I : YOU, AUDIENCE, GAP, RESOLUTION, HOW (call to arms) (2 mins)

- [ ] ACT II: (break in to 3 parts / key outcomes / stages) (12 mins, 4 mins each part)
 - [ ] Part I: identify requirements/scope (jenkins, github data, surveys)
 - [ ] Part II: development of tools / techniques / data collection / analysis method (but not result)
   - [ ] charts, log processing, images, survey data (raw numbers)
 - [ ] Part III: results! - I found that ... We can see ... (team, person, roles ?)

- [ ] ACT III: (recap: climax, resolution, call to arms - let's do it) (1 mins)


> Behaivour of teams and use of tools
