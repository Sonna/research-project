# Research Ideas

## The setup

The initial idea of the setup would to be host the **Continuous Integration** solution on either:

- A Mac Mini, or
- In the cloud via Amazon Web Services (AWS)

The main reason for hosting on a Mac mini computer would be to allow for Mac OS X and iOS project to be compiled via Xcode (or an alternative compiler). However, it also allows for easier installation of compilers and tools required of a "Continuous Integration" server; e.g. with [_homebrew_][Homebrew] Jenkins can be installed with one line.

``` shell
> brew install jenkins
```

Also, with the Mac mini hardware it would allow for move advance setups like [**Hypervisors**][Hypervisor], without legitimately sacrificing the ability to run the Mac OS X. [^virtual Mac OS X]

It is possible to run a **Hypervisor**, such as VMware's [ESXi], on Apple hardware and allow for the legitimate visualization of Apple operating systems. In this kind of setup I would propose that hardware would enough RAM to allow for multiple concurrent systems; e.g.

| OS | Cores | RAM | Objective |
-----|-------|-----|------------
| CentOS | 1-2 | ~4-8GB | To host either Jenkins, Repositories and/or a Web server |
| Windows | 1 | ~4GB | To compile Windows specific projects |
| Mac OS X | 1-2 | ~4GB | To compile Mac specific projects |

- Either Mac OS X or Windows would host Unity to compile those kinds of projects (because there is currently no Linux variant)

The alternative AWS would allow for similar Linux environment.. # TODO


## Possible Questions


**SWE30004 Software Deployment and Evolution** Lecture 01 - Introduction

**Slide 54**

> ## EBSE - Global Software Teams
>
> - Global Software teams (aka _Bangalored_)
>
> - Analysis of ~ 360 projects shows
>
>   - Distributed teams tend to produce low quality
>   - Severe imbalance in team impacts on quality (reduces it)
>   - Adding multiple development teams increases productivity (more features), costs more (reduces profit) & reduces quality
>   - Spread of experience (balanced team) reduces productivity, improves quality (but costs more).


**Slide 55**

> ## EBSE - Distributed Development
>
> - Building a team around an end-to-end feature (across modules/tiers) produces better quality and a higher throughput (_Cataldo 2008_)
> - Continuous integration improves overall productivity (works around Conway’s law)
> - Continuous deployment adds to the gains of Continuous integration
>   - CI+CD reduce this dialog:
>   > “it worked on my computer, must be something on your side”


<!-- # footnotes -->

[^virtual Mac OS X]: It is illegal to run Mac OS X in a virtual machine on a non-Apple hardware.


<!-- # References -->

[Homebrew]: http://brew.sh/
[Hypervisor]: http://en.wikipedia.org/wiki/Hypervisor
[ESXi]: http://www.vmware.com/products/vsphere-hypervisor
