<!-- James Kernaghan -->
# Developer One (of Team 1)


## Worklog Analysis

![Developer One's worklog as a calendar heatmap][Developer One - Worklog heatmap]

![Developer One's worklog task hours as they accumulated over the year][Developer One - Worklog accumulated hours]


<!--- # Images -->

[Developer One - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(james_kernaghan_-_semester_1).png "Developer One's Worklog - Calendar Heatmap"
[Developer One - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(james_kernaghan_-_semester_1).png "Developer One's Worklog - Acculumated hours"

