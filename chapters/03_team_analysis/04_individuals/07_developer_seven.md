<!-- Anthony Chenevier -->
# Developer Seven (of Team 3)

## Worklog Analysis

![Developer Seven's worklog as a calendar heatmap][Developer Seven - Worklog heatmap]

![Developer Seven's worklog task hours as they accumulated over the year][Developer Seven - Worklog accumulated hours]

![Developer Seven's programming skills as a Radar chart][Developer Seven - Survey Developer skills]

<!--- # Images -->

[Developer Seven - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(anthony_chenevier_-_semester_1_and_2).png "Developer Seven's Worklog - Calendar Heatmap"
[Developer Seven - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(anthony_chenevier_-_semester_1).png "Developer Seven's Worklog - Calendar Heatmap"
[Developer Seven - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(anthony_chenevier_-_semester_2).png "Developer Seven's Worklog - Calendar Heatmap"

[Developer Seven - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(anthony_chenevier_-_semester_1_and_2).png "Developer Seven's Worklog - Acculumated hours"
[Developer Seven - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(anthony_chenevier_-_semester_1).png "Developer Seven's Worklog - Acculumated hours"
[Developer Seven - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(anthony_chenevier_-_semester_2).png "Developer Seven's Worklog - Acculumated hours"

[Developer Seven - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_seven).png "Developer Seven's programming skills as a Radar chart"
