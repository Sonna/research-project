<!-- Johnathon McDonnell-Parker -->
# Developer Six (of Team 2)

## Worklog Analysis

![Developer Six's worklog as a calendar heatmap][Developer Six - Worklog heatmap]

![Developer Six's worklog task hours as they accumulated over the year][Developer Six - Worklog accumulated hours]


<!--- # Images -->

[Developer Six - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(johnathon_mcdonnell-parker_-_semester_1_and_2).png "Developer Six's Worklog - Calendar Heatmap"
[Developer Six - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(johnathon_mcdonnell-parker_-_semester_1).png "Developer Six's Worklog - Calendar Heatmap"
[Developer Six - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(johnathon_mcdonnell-parker_-_semester_2).png "Developer Six's Worklog - Calendar Heatmap"

[Developer Six - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(johnathon_mcdonnell-parker_-_semester_1_and_2).png "Developer Six's Worklog - Acculumated hours"
[Developer Six - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(johnathon_mcdonnell-parker_-_semester_1).png "Developer Six's Worklog - Acculumated hours"
[Developer Six - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(johnathon_mcdonnell-parker_-_semester_2).png "Developer Six's Worklog - Acculumated hours"

