<!-- Christopher Forrest -->
# Developer Nine (of Team 3)

## Worklog Analysis

![Developer Nine's worklog as a calendar heatmap][Developer Nine - Worklog heatmap]

![Developer Nine's worklog task hours as they accumulated over the year][Developer Nine - Worklog accumulated hours]

![Developer Nine's programming skills as a Radar chart][Developer Nine - Survey Developer skills]

<!--- # Images -->

[Developer Nine - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(christopher_forrest_-_semester_1_and_2).png "Developer Nine's Worklog - Calendar Heatmap"
[Developer Nine - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(christopher_forrest_-_semester_1).png "Developer Nine's Worklog - Calendar Heatmap"
[Developer Nine - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(christopher_forrest_-_semester_2).png "Developer Nine's Worklog - Calendar Heatmap"

[Developer Nine - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(christopher_forrest_-_semester_1_and_2).png "Developer Nine's Worklog - Acculumated hours"
[Developer Nine - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(christopher_forrest_-_semester_1).png "Developer Nine's Worklog - Acculumated hours"
[Developer Nine - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(christopher_forrest_-_semester_2).png "Developer Nine's Worklog - Acculumated hours"

[Developer Nine - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_nine).png "Developer Nine's programming skills as a Radar chart"
