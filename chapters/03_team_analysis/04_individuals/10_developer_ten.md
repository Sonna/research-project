<!-- Alex Sonneveld -->
# Developer Ten (of Team 4)

## Worklog Analysis

![Developer Ten's worklog as a calendar heatmap][Developer Ten - Worklog heatmap]

![Developer Ten's worklog task hours as they accumulated over the year][Developer Ten - Worklog accumulated hours]

<!-- ![Developer Ten's programming skills as a Radar chart][Developer Ten - Survey Developer skills] -->

<!--- # Images -->

[Developer Ten - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(alex_sonneveld_-_semester_1_and_2).png "Developer Ten's Worklog - Calendar Heatmap"
[Developer Ten - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(alex_sonneveld_-_semester_1).png "Developer Ten's Worklog - Calendar Heatmap"
[Developer Ten - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(alex_sonneveld_-_semester_2).png "Developer Ten's Worklog - Calendar Heatmap"

[Developer Ten - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(alex_sonneveld_-_semester_1_and_2).png "Developer Ten's Worklog - Acculumated hours"
[Developer Ten - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(alex_sonneveld_-_semester_1).png "Developer Ten's Worklog - Acculumated hours"
[Developer Ten - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(alex_sonneveld_-_semester_2).png "Developer Ten's Worklog - Acculumated hours"

<!-- [Developer Ten - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_ten).png "Developer Ten's programming skills as a Radar chart" -->
