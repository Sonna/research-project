<!-- Milton Plotkin -->
# Developer Two (of Team 1)

## Worklog Analysis

![Developer Two's worklog as a calendar heatmap][Developer Two - Worklog heatmap]

![Developer Two's worklog task hours as they accumulated over the year][Developer Two - Worklog accumulated hours]

![Developer Two's programming skills as a Radar chart][Developer Two - Survey Developer skills]

<!--- # Images -->

[Developer Two - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(milton_plotkin_-_semester_1_and_2).png "Developer Two's Worklog - Calendar Heatmap"
[Developer Two - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(milton_plotkin_-_semester_1).png "Developer Two's Worklog - Calendar Heatmap"
[Developer Two - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(milton_plotkin_-_semester_2).png "Developer Two's Worklog - Calendar Heatmap"

[Developer Two - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(milton_plotkin_-_semester_1_and_2).png "Developer Two's Worklog - Acculumated hours"
[Developer Two - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(milton_plotkin_-_semester_1).png "Developer Two's Worklog - Acculumated hours"
[Developer Two - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(milton_plotkin_-_semester_2).png "Developer Two's Worklog - Acculumated hours"

[Developer Two - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_two).png "Developer Two's programming skills as a Radar chart"
