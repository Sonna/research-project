<!-- Hadleigh Barton-Ancliffe -->
# Developer Eleven (of Team 4)

## Worklog Analysis

![Developer Eleven's worklog as a calendar heatmap][Developer Eleven - Worklog heatmap]

![Developer Eleven's worklog task hours as they accumulated over the year][Developer Eleven - Worklog accumulated hours]

![Developer Eleven's programming skills as a Radar chart][Developer Eleven - Survey Developer skills]

<!--- # Images -->

[Developer Eleven - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(hadleigh_barton-ancliffe_-_semester_1_and_2).png "Developer Eleven's Worklog - Calendar Heatmap"
[Developer Eleven - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(hadleigh_barton-ancliffe_-_semester_1).png "Developer Eleven's Worklog - Calendar Heatmap"
[Developer Eleven - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(hadleigh_barton-ancliffe_-_semester_2).png "Developer Eleven's Worklog - Calendar Heatmap"

[Developer Eleven - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(hadleigh_barton-ancliffe_-_semester_1_and_2).png "Developer Eleven's Worklog - Acculumated hours"
[Developer Eleven - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(hadleigh_barton-ancliffe_-_semester_1).png "Developer Eleven's Worklog - Acculumated hours"
[Developer Eleven - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(hadleigh_barton-ancliffe_-_semester_2).png "Developer Eleven's Worklog - Acculumated hours"

[Developer Eleven - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_eleven).png "Developer Eleven's programming skills as a Radar chart"
