<!-- Ryan Bowen -->
# Developer Three (of Team 1)

## Worklog Analysis

![Developer Three's worklog as a calendar heatmap][Developer Three - Worklog heatmap]

![Developer Three's worklog task hours as they accumulated over the year][Developer Three - Worklog accumulated hours]


<!--- # Images -->

[Developer Three - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_bowen_-_semester_1_and_2).png "Developer Three's Worklog - Calendar Heatmap"
[Developer Three - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_bowen_-_semester_1).png "Developer Three's Worklog - Calendar Heatmap"
[Developer Three - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_bowen_-_semester_2).png "Developer Three's Worklog - Calendar Heatmap"

[Developer Three - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_bowen_-_semester_1_and_2).png "Developer Three's Worklog - Acculumated hours"
[Developer Three - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_bowen_-_semester_1).png "Developer Three's Worklog - Acculumated hours"
[Developer Three - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_bowen_-_semester_2).png "Developer Three's Worklog - Acculumated hours"

