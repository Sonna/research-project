<!-- Ryan Byles -->
# Developer Four (of Team 2)

## Worklog Analysis

![Developer Four's worklog as a calendar heatmap][Developer Four - Worklog heatmap]

![Developer Four's worklog task hours as they accumulated over the year][Developer Four - Worklog accumulated hours]

![Developer Four's programming skills as a Radar chart][Developer Four - Survey Developer skills]

<!--- # Images -->

[Developer Four - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_byles_-_semester_1_and_2).png "Developer Four's Worklog - Calendar Heatmap"
[Developer Four - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_byles_-_semester_1).png "Developer Four's Worklog - Calendar Heatmap"
[Developer Four - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_byles_-_semester_2).png "Developer Four's Worklog - Calendar Heatmap"

[Developer Four - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_byles_-_semester_1_and_2).png "Developer Four's Worklog - Acculumated hours"
[Developer Four - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_byles_-_semester_1).png "Developer Four's Worklog - Acculumated hours"
[Developer Four - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_byles_-_semester_2).png "Developer Four's Worklog - Acculumated hours"

[Developer Four - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_four).png "Developer Four's programming skills as a Radar chart"
