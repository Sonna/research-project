<!-- Ryan Hisheh -->
# Developer Five (of Team 2)

## Worklog Analysis

![Developer Five's worklog as a calendar heatmap][Developer Five - Worklog heatmap]

![Developer Five's worklog task hours as they accumulated over the year][Developer Five - Worklog accumulated hours]

![Developer Five's programming skills as a Radar chart][Developer Five - Survey Developer skills]

<!--- # Images -->

[Developer Five - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_hisheh_-_semester_1_and_2).png "Developer Five's Worklog - Calendar Heatmap"
[Developer Five - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_hisheh_-_semester_1).png "Developer Five's Worklog - Calendar Heatmap"
[Developer Five - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ryan_hisheh_-_semester_2).png "Developer Five's Worklog - Calendar Heatmap"

[Developer Five - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_hisheh_-_semester_1_and_2).png "Developer Five's Worklog - Acculumated hours"
[Developer Five - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_hisheh_-_semester_1).png "Developer Five's Worklog - Acculumated hours"
[Developer Five - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ryan_hisheh_-_semester_2).png "Developer Five's Worklog - Acculumated hours"

[Developer Five - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_five).png "Developer Five's programming skills as a Radar chart"
