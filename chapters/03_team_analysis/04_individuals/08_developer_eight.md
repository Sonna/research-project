<!-- Emile Pascoe -->
# Developer Eight (of Team 3)

## Worklog Analysis

![Developer Eight's worklog as a calendar heatmap][Developer Eight - Worklog heatmap]

![Developer Eight's worklog task hours as they accumulated over the year][Developer Eight - Worklog accumulated hours]

![Developer Eight's programming skills as a Radar chart][Developer Eight - Survey Developer skills]

<!--- # Images -->

[Developer Eight - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(emile_pascoe_-_semester_1_and_2).png "Developer Eight's Worklog - Calendar Heatmap"
[Developer Eight - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(emile_pascoe_-_semester_1).png "Developer Eight's Worklog - Calendar Heatmap"
[Developer Eight - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(emile_pascoe_-_semester_1).png "Developer Eight's Worklog - Calendar Heatmap"

[Developer Eight - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(emile_pascoe_-_semester_1_and_2).png "Developer Eight's Worklog - Acculumated hours"
[Developer Eight - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(emile_pascoe_-_semester_1).png "Developer Eight's Worklog - Acculumated hours"
[Developer Eight - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(emile_pascoe_-_semester_2).png "Developer Eight's Worklog - Acculumated hours"

[Developer Eight - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_eight).png "Developer Eight's programming skills as a Radar chart"
