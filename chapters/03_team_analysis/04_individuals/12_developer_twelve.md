<!-- Ciaran Blewitt -->
# Developer Twelve (of Team 4)

## Worklog Analysis

![Developer Twelve's worklog as a calendar heatmap][Developer Twelve - Worklog heatmap]

![Developer Twelve's worklog task hours as they accumulated over the year][Developer Twelve - Worklog accumulated hours]

![Developer Twelve's programming skills as a Radar chart][Developer Twelve - Survey Developer skills]

<!--- # Images -->

[Developer Twelve - Worklog heatmap]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ciaran_blewitt_-_semester_1_and_2).png "Developer Twelve's Worklog - Calendar Heatmap"
[Developer Twelve - Worklog heatmap - Semester 1]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ciaran_blewitt_-_semester_1).png "Developer Twelve's Worklog - Calendar Heatmap"
[Developer Twelve - Worklog heatmap - Semester 2]: images/worklogs/calendar_heatmaps/calendar_heatmap_(ciaran_blewitt_-_semester_2).png "Developer Twelve's Worklog - Calendar Heatmap"

[Developer Twelve - Worklog accumulated hours]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ciaran_blewitt_-_semester_1_and_2).png "Developer Twelve's Worklog - Acculumated hours"
[Developer Twelve - Worklog accumulated hours - Semester 1]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ciaran_blewitt_-_semester_1).png "Developer Twelve's Worklog - Acculumated hours"
[Developer Twelve - Worklog accumulated hours - Semester 2]: images/worklogs/accumulated_hours/accumulated_hours_over_the_semester_(ciaran_blewitt_-_semester_2).png "Developer Twelve's Worklog - Acculumated hours"

[Developer Twelve - Survey Developer skills]: images/survey/programming_skills/radar_chart_(developer_twelve).png "Developer Twelve's programming skills as a Radar chart"
