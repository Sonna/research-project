# Basic Data (with parameters)

_This section to begin the analysis of the current team dynamics and their use of the version control systems, whilst providing a raw data set from the survey responses._

_This sections should include:_
- Raw data,
- the user group (demographic),
- etc
