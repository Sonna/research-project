# Swing Kings

_This team consists of three developers, one of which left after the first
semester, and will be referred to as developers One, Two and Three._

## GitHub graph data

The following sections contain assumptions that being construed from the graphed repository information.

### Contributions

![SwingKings Contributions - Commits][]

![SwingKings Contributions - Additions][]

![SwingKings Contributions - Deletions][]


### Commits

![SwingKings Commits][]

There are two key main factors that be seen clearing from this graph:

1. The majority of work is committed on Mondays (during the week of September 21), and
2. Little to no work was done between 08 June and 20 July; i.e. the semester break

_Work being committed on a Monday coincides with the weekly team meeting during the first semester._

The other notable thing is that the majority occurred during the first semester (23 March to 08 June) and not nearly as much during the second semester (20 July to 21 September). There is also a brief gap between 07 September and 21 September, which would indicate the mid-semester break.

From this information it can be argued that this team choses to not work during "holidays" or when away from university for extended periods of time.

_Potentially you may argue that the revision control tool is not tracking their total amount of work, because it only tracks what is committed to the project repository. The same could said for work that not been "pushed" to remote GitHub repository and there could still be work left untracked on a contributors local computer._

Also, some facts about amount the commits:

1. 25 commits were committed during the week of April 27,
2. The team was approximately committing at least 10 or more commits each week during the first semester, but this reduced to less 10 during the second semester (expect for the spike of 25 commits during week of September 7)


### Code Frequency

![SwingKings Code Frequency][]

_Due to the nature of using Git version control system with Unity3D these graphs on additions and deletions will swing (no pun intended) wildly because of large assets such as, models, textures, sound effects, music or Unity scenes files. Unity3D scene files are large text files (or binary depending on Project Settings) which contain serializations assets or GameObjects within a scene. These often change and those changes are not well defined, which often leads to large modifications or in the case of these graphs large additions and deletions._

- During the first semester it is evident that new assets were committed and a large amount of modification occurred to them during weeks four and five,
- Work almost completely stopped during the semester break or was not committed until the team return after the mid-semester break,
- A large amount of new content was committed during the second half of first semester,
- No work was done during the break between semesters (and based off previous activity no additional work was committed upon return, thus no work was actually done)


### Punchcards

![SwingKings Punchcard\label{sk_punchcard}][SwingKings Punchcard]

It would appear from this graph, in figure \ref{sk_punchcard} above, that Team 1 "Swing Kings" are only working within the time they are allocated use of the AS building bunker room / computer lab. This is unfortunate, because they are not actively working this project outside this time on campus (or are possibly unable to?).

Alternatively, because there is still work, but less frequent work, outside the allocated lab time it could mean that work is slower when team members are apart. However, this also untrue because all the commits that occur outside the allocate time were only one commit occurrences and were only being performed by contributor.

![Developer commits frequently][SwingKings Contributor 01]

![Developer commits weekly][SwingKings Contributor 02]

![Developer commits weekly][SwingKings Contributor 03]

_A quick visual comparison of commit dates by each contributor._


That contributor was later removed from team due to them negatively contributing; i.e. removing content without consent or legitimate reasons.

![SwingKings Negative Contributor][]


<!--- # Images -->

[SwingKings Contributions - Commits]: images/github_graphs/contributions/screenshot_2014-09-22_22.32.55.png "SwingKings GitHub Contributions - Commits"
[SwingKings Contributions - Additions]: images/github_graphs/contributions/screenshot_2014-09-22_22.35.58.png "SwingKings GitHub Contributions - Additions"
[SwingKings Contributions - Deletions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.03.png "SwingKings GitHub Contributions - Deletions"

[SwingKings Commits]: images/github_graphs/commits/screenshot_2014-09-22_23.10.10.png "Swing Kings GitHub Commits"

[SwingKings Code Frequency]: images/github_graphs/frequency/screenshot_2014-09-22_22.38.20.png "Swing Kings GitHub Code Frequency"

[SwingKings Punchcard]: images/github_graphs/punch_card/screenshot_2014-09-22_22.39.40.png "Swing Kings GitHub Punchcard"
[SwingKings Negative Contributor]: images/github_graphs/punch_card/screenshot_2014-09-22_22.54.33.png "Swing Kings GitHub Negative Contributor"

<!--- _Swing Kings recent commits for comparisons._ -->

[SwingKings Contributor 01]: images/github_graphs/swing_kings/screenshot_2014-09-22_22.56.22.png "Swing Kings GitHub Contributor 01"
[SwingKings Contributor 02]: images/github_graphs/swing_kings/screenshot_2014-09-22_22.56.26.png "Swing Kings GitHub Contributor 02"
[SwingKings Contributor 03]: images/github_graphs/swing_kings/screenshot_2014-09-22_22.56.29.png "Swing Kings GitHub Contributor 03"

<!--- # References -->

[Swinburne academic calendar]: http://www.swinburne.edu.au/student-administration/calendar/


