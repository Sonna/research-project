# Subject Zero

_This team consists of three developers and will be referred to as developers
Ten, Eleven and Twelve._

## GitHub graph data

The following sections contain assumptions that being construed from the graphed repository information.

### Contributions

![SubjectZero Contributions - Commits][]

![SubjectZero Contributions - Additions][]

![SubjectZero Contributions - Deletions][]


### Commits

Also, some facts about amount the commits:

1. 113 commits were committed during the week of April 16,


### Code Frequency

### Punchcards

![SubjectZero Punchcard][]

_The largest dots on this graph range between 40-60 commits. The largest commit is around 70 commits. Smaller dots are less 20 commits and the smallest are roughly 2 commits._

It would appear from this graph above that Team 4 "Subject Zero" work most often on Mondays, Wednesdays and Thursdays. Any other days during week are either infrequent worked within or not much is committed during those times. This is interesting because it means that Team 4 is active during weekdays than during the weekend. Also, this team is not locked into their weekly allocated time for the AS building bunker room / computer lab (like team 1), which was Wednesday during first semester and Thursday during second semester.

Most commits occurs between the hours of 9am and 6pm. Only a few commits occurred Monday evenings and one out lier occurred at 12am Saturday morning.

~~The 11 commits that occurred Saturday morning during the week of ?~~


<!--- # Images -->

[SubjectZero Contributions - Commits]: images/github_graphs/contributions/screenshot_2014-09-22_22.33.43.png "SubjectZero GitHub Contributions - Commits"
[SubjectZero Contributions - Additions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.17.png "SubjectZero GitHub Contributions - Additions"
[SubjectZero Contributions - Deletions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.19.png "SubjectZero GitHub Contributions - Deletions"

[SubjectZero Punchcard]: images/github_graphs/punch_card/screenshot_2014-09-22_22.39.45.png "SubjectZero GitHub Punchcard"


<!--- # References -->



