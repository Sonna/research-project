# Magnicity

_This team consists of three developers and will be referred to as developers
Four, Five and Six._

## GitHub graph data

The following sections contain assumptions that being construed from the graphed repository information.

### Contributions

![Magnicity Contributions - Commits][]

![Magnicity Contributions - Additions][]

![Magnicity Contributions - Deletions][]


### Commits

![Magnicity Commits][]

The two main factors that can be seen from this graph are:

1. The team does not commit frequently, suggesting they do not use version control "correctly" or as effectively as other teams, and
2. The majority of work is committed on a Wednesday (during the week of September 21)

_Work being committed on a Wednesday coincides with the weekly team meeting during the first semester._

Observations about the amount of commits:

- There 13 commits total between 23 March and 21 September (or 26 weeks / 182 days), which averages out to about approximately ~.07 commits per day. (_Arguably not a good measurement._)


Also, some facts about amount the commits:

1. 6 commits were committed during the week of April 6,


### Code Frequency

![Magnicity Code Frequency][]

_Due to the nature of using Git version control system with Unity3D these graphs on additions and deletions will swing (no pun intended) wildly because of large assets such as, models, textures, sound effects, music or Unity scenes files. Unity3D scene files are large text files (or binary depending on Project Settings) which contain serializations assets or GameObjects within a scene. These often change and those changes are not well defined, which often leads to large modifications or in the case of these graphs large additions and deletions._

- During the first semester it is evident that new assets were committed and a large amount of modification occurred to them during weeks four and five,
- Work almost completely stopped during the semester break or was not committed until the team needed to submit their repository for assessment,
- A large amount of new content was committed during the end of first semester and was done so over 6 commits (committed by one user; i.e. "CrimsonBinome")
- No work was done during the break between semesters (and based off previous activity no additional work was committed until assessments needed to submitted, thus no work was actually done)

### Punchcards

![Magnicity Punchcard][]

The major problem with this graph is that it hard to extrapolate any useful information from it because there are not enough data points to say it strongly suggests one or other argument.

From the data available, the team works (or commits) often occur during midday into the afternoon and "usually" on a Sunday or Wednesday. However, because there are only two or three commits for each day there is a commit the previous may not be true.

![Magnicity Commits - April 6][]

When comparing the punchcard graph to the commits per week graph, during the week of April 6, those 6 commits occurred over the Sunday, Monday and once on Tuesday. Given that is commit spike is uncommon event, it would say to say that team commits (and possibly work) occur on Wednesdays. However, when comparing to other commits per weeks across the lifetime of the project, it becomes evident that there is no frequent occurrence and that team commits at what appears to be randomly.

![Magnicity Commits - March 23][]

![Magnicity Commits - April 13][]

![Magnicity Commits - April 20][]

![Magnicity Commits - June 15][]

![Magnicity Commits - August 31][]

![Magnicity Commits - September 28][]

There often only one commit and that one commit often occurs weeks a part. The spike during the week of April 6 is only out lier.

This analysis compares the commit punchcard to the commits per week to find that there is no pattern between to know when Team 2 will commit to their project.


<!--- # Images -->

[Magnicity Contributions - Commits]: images/github_graphs/contributions/screenshot_2014-09-22_22.33.36.png "Magnicity GitHub Contributions - Commits"
[Magnicity Contributions - Additions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.06.png "Magnicity GitHub Contributions - Additions"
[Magnicity Contributions - Deletions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.08.png "Magnicity GitHub Contributions - Deletions"

[Magnicity Commits]: images/github_graphs/commits/screenshot_2014-09-22_23.10.13.png "Magnicity GitHub Commits"

[Magnicity Code Frequency]: images/github_graphs/frequency/screenshot_2014-09-22_22.38.23.png "Magnicity GitHub Code Frequency"

[Magnicity Punchcard]: images/github_graphs/punch_card/screenshot_2014-09-22_22.39.41.png "Magnicity GitHub Punchcard"

_Commits per week frequency._

[Magnicity Commits - March 23]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.48.png "Magnicity GitHub Magnicity Commits for the week of April 6"
[Magnicity Commits - April 6]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.54.png "Magnicity GitHub Magnicity Commits for the week of "
[Magnicity Commits - April 13]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.50.png "Magnicity GitHub Magnicity Commits for the week of "
[Magnicity Commits - April 20]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.56.png "Magnicity GitHub Magnicity Commits for the week of "
[Magnicity Commits - June 15]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.51.png "Magnicity GitHub Magnicity Commits for the week of "
[Magnicity Commits - August 31]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.59.png "Magnicity GitHub Magnicity Commits for the week of "
[Magnicity Commits - September 28]: images/github_graphs/magnicity/commits/screenshot_2014-10-20_12.14.53.png "Magnicity GitHub Magnicity Commits for the week of "


<!--- # References -->

