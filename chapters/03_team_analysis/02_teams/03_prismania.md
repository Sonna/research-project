# Prismania

_This team consists of three developers and will be referred to as developers
Seven, Eight and Nine._

## GitHub graph data

The following sections contain assumptions that being construed from the graphed repository information.

### Contributions

![Prismania Contributions - Commits][]

![Prismania Contributions - Additions][]

![Prismania Contributions - Deletions][]


### Commits

![Prismania Commits][]

Also, some facts about amount the commits:

1. 30 commits were committed during the week of June 1,


### Code Frequency

![Prismania Code Frequency][]


### Punchcards

![Prismania Punchcard][]


<!--- # Images -->

[Prismania Contributions - Commits]: images/github_graphs/contributions/screenshot_2014-09-22_22.33.40.png "Prismania GitHub Contributions - Commits"
[Prismania Contributions - Additions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.06.png "Prismania GitHub Contributions - Additions"
[Prismania Contributions - Deletions]: images/github_graphs/contributions/screenshot_2014-09-22_22.36.08.png "Prismania GitHub Contributions - Deletions"


[Prismania Commits]: images/github_graphs/commits/screenshot_2014-09-22_23.10.16.png "Prismania GitHub Commits"

[Prismania Code Frequency]: images/github_graphs/frequency/screenshot_2014-09-22_22.38.25.png "Prismania GitHub Code Frequency"

[Prismania Punchcard]: images/github_graphs/punch_card/screenshot_2014-09-22_22.39.43.png "Prismania GitHub Punchcard"


<!--- # References -->



