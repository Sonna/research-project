# Individual Analysis

This section to begin the analysis of the individuals within each team and their
use of the version control systems, project management tools and work-logs,
whilst providing a raw data set from the survey responses.

This sections includes:
- Raw data,
- the user group (demographic),
- etc


_Throughout this document each developer is referred to by their randomly
assigned number, but each factor of three belongs to that respective team; e.g.
developers One, Two & Three belong to Team 1, whilst developers Four, Five & Six
belong to Team 2. This is because there were four teams each with three
developers assigned to them._
